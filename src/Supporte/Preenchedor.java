/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Supporte;

import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.bean.Predio;
import model.bean.Setor;
import model.dao.PredioDAO;
import model.dao.SetorDAO;

/**
 *
 * @author Maurício
 */
public class Preenchedor {
    public static void PreencherJTableSetorEPredio(JTable jt){
        ArrayList<Predio> predios;
        DefaultTableModel modelo = (DefaultTableModel) jt.getModel();
        predios = PredioDAO.busca();
        modelo.setNumRows(0);
        for (Predio p: predios){
            modelo.addRow(new Object[]{
                SetorDAO.returnSet(p.getIdSetor()).getNome(),
                p.getNome()
            });
        }
        }      
    
    public static void PreencherJTableSetor(JTable jt){
        ArrayList<Setor> setores;
        DefaultTableModel modelo = (DefaultTableModel) jt.getModel();
        setores = SetorDAO.buscaSetoresComPredios();
        modelo.setNumRows(0);
        for (Setor s: setores){
            modelo.addRow(new Object[]{
                s.getNome()
            });
        }
        }  
    
    public static void PreencherJComboBoxSetor(JComboBox jc){
        ArrayList<Setor> setores;
        setores = SetorDAO.busca();
        DefaultComboBoxModel comboModel = (DefaultComboBoxModel) jc.getModel();
        jc.setModel(comboModel);
        for (Setor s: setores){
           comboModel.addElement(s.getNome());
        }
    }
}

