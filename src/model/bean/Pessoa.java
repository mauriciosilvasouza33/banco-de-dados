/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

/**
 *
 * @author Maurício
 */
public class Pessoa {
    
    private int idPessoa;
    private String cpf;
    private String rg;
    private String nome;
    private String sobrenome;
    private String religiao;
    private String estadoCivil;
    private String telefone;
    private String email;
    private String escolaridade;
    private int idEndereco;
    private Endereco endereco;

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
    

    public void setIdPessoa(int idPessoa) {
        this.idPessoa = idPessoa;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public void setReligiao(String religiao) {
        this.religiao = religiao;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setEscolaridade(String escolaridade) {
        this.escolaridade = escolaridade;
    }

    public void setIdEndereco(int idEndereco) {
        this.idEndereco = idEndereco;
    }

    
    
    public int getIdPessoa() {
        return idPessoa;
    }

    public String getCpf() {
        return cpf;
    }

    public String getRg() {
        return rg;
    }

    public String getNome() {
        return nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public String getReligiao() {
        return religiao;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getEmail() {
        return email;
    }

    public String getEscolaridade() {
        return escolaridade;
    }

    public int getIdEndereco() {
        return idEndereco;
    }
    
    
    
}
