/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

/**
 *
 * @author Maurício
 */
public class Ronda {
    
    private Turno data;
    private Turno idVigilante;
    private Setor idSetor;

    public Turno getData() {
        return data;
    }

    public void setData(Turno data) {
        this.data = data;
    }

    public Turno getIdVigilante() {
        return idVigilante;
    }

    public void setIdVigilante(Turno idVigilante) {
        this.idVigilante = idVigilante;
    }

    public Setor getIdSetor() {
        return idSetor;
    }

    public void setIdSetor(Setor idSetor) {
        this.idSetor = idSetor;
    }
    
    
    
}
