/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

/**
 *
 * @author Maurício
 */
public class Turno {
    
    private String data;
    private String periodo;
    private Vigilante idVigilante;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public Vigilante getIdVigilante() {
        return idVigilante;
    }

    public void setIdVigilante(Vigilante idVigilante) {
        this.idVigilante = idVigilante;
    }
    
    
    
}
