/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import connection.ConnectionFactory;
import java.awt.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.bean.Cidade;

/**
 *
 * @author Maurício
 */
public class CidadeDAO  {

    ArrayList<Cidade> cidades = new ArrayList<>();
    private static int lastid=0;

    public static int getLastid() {
        return lastid;
    }
    
//    Procurando id
//    public static Cidade busca(Cidade cid) {
//        Connection con = ConnectionFactory.getConnection();
//        PreparedStatement stmt = null;
//        ResultSet rs = null;
//        String sql = "SELECT * FROM cidade WHERE nome = '" + cid.getNome() + "'AND uf = '" + cid.getUf() + "'";
//
//        try {
//            stmt = con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
//            stmt.executeQuery();
//            rs = stmt.getGeneratedKeys();
//            if (rs.next()) {
//                final int lastId = rs.getInt("");
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(CidadeDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return cid;
//    }

    
    public boolean inserir(Object obj) {
        Cidade ci = (Cidade) obj;
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        String sql = "INSERT INTO cidade(nomeCid, uf) values (?, ?)";

        try {
            //Preparando statement e retornando a ultima chave primaria inserida
            stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            
            //Setando os valores
            stmt.setString(1, ci.getNome());
            stmt.setString(2, ci.getUf());
            
            //Execuntando comando de inserção no banco
            stmt.executeUpdate();
            
            //Pegando as chaves
            ResultSet rs = stmt.getGeneratedKeys();
            
            //Pegando a ultima chave inserida
            if (rs.next()) {
                lastid = rs.getInt(1);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao salvar dados de cidade");
            return false;
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
        return true;
    }
    
    public static void delete(){
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        String sql = "DELETE FROM cidade WHERE idCidade = " + lastid;
        
        try {
            stmt = con.prepareStatement(sql);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao apagar dados de cidade");
        }
        
    }
    
}
