/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import model.bean.Contrato;

/**
 *
 * @author Maurício
 */
public class ContratoDAO {

    public boolean inserir(double salario, int idVigilante) {

        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        String sql = "INSERT INTO contrato (idVigilante, salario) values (?, ?)";

        try {
            stmt = con.prepareStatement(sql);

            stmt.setInt(1, idVigilante);
            stmt.setDouble(2, salario);

            stmt.executeUpdate();

        }catch(NumberFormatException ne){
            JOptionPane.showMessageDialog(null, "Não foi possível inserir dados do vigilante");
            return false;
        } 
        catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possível inserir dados do vigilante");
            return false;
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
        return true;
    }

    //Buscando todos os dados do contrato do vigilante 
    public static Contrato buscaContrato(int idPessoa) {

        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        ResultSet rs;
        String sql = "SELECT * FROM contrato WHERE idVigilante = " + idPessoa + " order by dataEntrada desc limit 1";
        Contrato c = new Contrato();

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            rs.next();

            //Formatando data recebida do banco
            Date dataEntrada = rs.getDate("dataEntrada");
            SimpleDateFormat formatador = new SimpleDateFormat("dd-MM-yyyy");
            if (rs.getDate("dataSaida") != null) {
                Date dataSaida = rs.getDate("dataSaida");
                c.setDataSaida(formatador.format(dataSaida));
            }
            c.setDataEntrada(formatador.format(dataEntrada));
            c.setSalario(rs.getDouble("salario"));

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possível pegar dados desse caboco");
        }
        return c;
    }

    //Buscando todos os dados do contrato do vigilante 
    public static Contrato buscaDemitidos(int idPessoa) {

        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        ResultSet rs;

        Date data = new Date();
        SimpleDateFormat formatador = new SimpleDateFormat("yyyy-MM-dd");

        String sql = "SELECT * FROM contrato WHERE idVigilante = " + idPessoa + " AND dataSaida = '" + formatador.format(data)+"'";
        Contrato c = new Contrato();
        SimpleDateFormat formatador2 = new SimpleDateFormat("dd-MM-yyyy");

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            rs.next();

            //Formatando data recebida do banco
            Date dataEntrada = rs.getDate("dataEntrada");
            Date dataSaida = rs.getDate("dataSaida");
            c.setDataSaida(formatador2.format(dataSaida));
            c.setDataEntrada(formatador2.format(dataEntrada));
            c.setSalario(rs.getDouble("salario"));

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possível pegar dados desse vigilante");
        }
        return c;
    }

    public static void updateContrato(int idPessoa, double salario) {

        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        
        String sql = "UPDATE contrato SET salario = ? WHERE idVigilante = " + idPessoa + " AND dataSaida is null";
        System.out.println(sql);
        try {
            stmt = con.prepareStatement(sql);
            stmt.setDouble(1, salario);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir novo salario");
        }

    }

    public static void demite(int idVigilante) {

        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        String sql = "UPDATE contrato SET dataSaida = ? WHERE idVigilante = " + idVigilante;

        Date data = new Date();
        SimpleDateFormat formatador = new SimpleDateFormat("yyyy-MM-dd");

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, formatador.format(data));
            stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao atualizar data de saída do vigilante");
        }

    }

    public static void readmite(double salario, int idVigilante) throws SQLException {

        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        String sql = "INSERT INTO contrato (salario, idVigilante) values(?, ?)";

        stmt = con.prepareStatement(sql);
        stmt.setDouble(1, salario);
        stmt.setInt(2, idVigilante);
        stmt.executeUpdate();
    }

    public static void delete(){
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        String sql = "DELETE FROM pessoa WHERE idpessoa = " + PessoaDAO.getLastid() ;
        
        try {
            stmt = con.prepareStatement(sql);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao apagar dados de pessoa");
        }
        
    }
    
}
