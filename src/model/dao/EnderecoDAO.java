package model.dao;

import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import model.bean.Endereco;

public class EnderecoDAO{
    private static int lastid;

    public static int getLastid() {
        return lastid;
    }

    public boolean inserir(Object obj) {
        
        Endereco end = (Endereco) obj;
        Connection con = ConnectionFactory.getConnection();
        
        
        PreparedStatement stmt = null;
        String sql = "INSERT INTO endereco(rua, bairro, numero, idCidade) values (?, ?, ?, ?)";
        
        try {
            stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            
            stmt.setString(1, end.getRua());
            stmt.setString(2, end.getBairro()); 
            stmt.setString(3, end.getNumero());
            stmt.setInt(4, end.getIdCidade());
            stmt.executeUpdate();
            
            ResultSet rs = stmt.getGeneratedKeys();
            
            
            if (rs.next()) {
                lastid = rs.getInt(1);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao salvar dados de endereço: ");
            return false;
        } finally{
            ConnectionFactory.closeConnection(con, stmt);
        }
        return true;
    }
    
    public static void delete(){
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        String sql = "DELETE FROM cidade WHERE idCidade = " + CidadeDAO.getLastid() ;
        
        try {
            stmt = con.prepareStatement(sql);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao apagar dados de endereco");
        }
        
    }
    
}
