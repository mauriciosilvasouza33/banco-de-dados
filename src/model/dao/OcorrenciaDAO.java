/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.bean.Ocorrencia;
import trabalhobanco.views.ManterTurnoView;

/**
 *
 * @author Maurício
 */
public class OcorrenciaDAO {

    //INSERINDO DADOS EM OCORRENCIA
    public static void inserir(Ocorrencia o) {
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        String sql = "INSERT INTO ocorrencia (descricao, idPredio, idSetor, data, idVigilante) values (?, ?, ?, ?, ?)";

        try {

            stmt = con.prepareStatement(sql);
            stmt.setString(1, o.getDescricao());
            stmt.setInt(2, o.getIdPredio());
            stmt.setInt(3, o.getIdSetor());
            stmt.setString(4, o.getData());
            stmt.setInt(5, o.getIdVigilante());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possível inserir a ocorrência");
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
    }

    //Buscando dados de ocorrência
    public static List<Ocorrencia> buscaOcorrencia() {

        Connection con = ConnectionFactory.getConnection();

        List<Ocorrencia> lista = new ArrayList<>();
        PreparedStatement stmt;
        ResultSet rs;
        String sql = "select p.idPessoa, pr.idSetor, o.idOcorrencia, p.nome as vigilante, p.cpf, o.data, t.periodo, pr.nome as predio, o.descricao  from pessoa p join turno t on p.idPessoa = t.idVigilante join ocorrencia o on t.idVigilante = o.idVigilante join predio pr on o.idPredio = pr.idPredio group by o.descricao, pr.nome order by o.data, t.periodo";

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Ocorrencia b = new Ocorrencia();

                b.setIdOcorrencia(rs.getInt("idOcorrencia"));
                b.setNome(rs.getString("vigilante"));
                b.setCpf(rs.getString("cpf"));
                b.setData(rs.getString("data"));
                b.setPeriodo(rs.getString("periodo"));
                b.setPredio(rs.getString("predio"));
                b.setDescricao(rs.getString("descricao"));
                b.setIdSetor(rs.getInt("idSetor"));
                b.setIdVigilante(rs.getInt("idPessoa"));

                lista.add(b);

            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ops! falha ao inserir dados na tabela de ocorrencia");
        }
        return lista;

    }

    //Inserindo dados na tabela
    public static void PreencherJTable(JTable jt, ManterTurnoView view) {
        List<Ocorrencia> b = OcorrenciaDAO.buscaOcorrencia();
        DefaultTableModel modelo = (DefaultTableModel) jt.getModel();
        modelo.setNumRows(0);

        for (Ocorrencia i : b) {
            modelo.addRow(new Object[]{
                i.getNome(),
                i.getCpf(),
                i.getData(),
                i.getPeriodo(),
                i.getPredio(),
                i.getDescricao()
            });
        }
    }

    //Fazendo update em ocorrencia
    public static void updateOcorrencia(Ocorrencia o) {

        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;

        //Setando dados da pessoa
        String sql = "update ocorrencia set descricao = ? where idOcorrencia = " + o.getIdOcorrencia();

        try {

            stmt = con.prepareCall(sql);
            stmt.setString(1, o.getDescricao());

            stmt.executeUpdate();

            JOptionPane.showMessageDialog(null, "Ocorrencia editada com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao tentar gravar dados de ocorrencia");
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
    }

    public static void deletaOcorrencia(String data, int idVigilante, int idSetor) throws SQLException {

        List<Ocorrencia> o = buscaOcorrencia();
        
        Connection con = ConnectionFactory.getConnection();

        PreparedStatement stmt;
        String sql = "DELETE FROM ocorrencia WHERE data = '" + data + "' AND idVigilante = "+ idVigilante +" AND idSetor = "+ idSetor;

        stmt = con.prepareStatement(sql);
        stmt.executeUpdate();
        
    }

    public static boolean analisaOcorrencias(String data, int idVigilante, int idSetor) {

        List<Ocorrencia> o = buscaOcorrencia();
        
        for(Ocorrencia p : o){
            if(p.getData().equals(data) && p.getIdVigilante() == idVigilante && p.getIdSetor() == idSetor){
                return true;
            }
        }

        return false;
    }

}
