/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import model.bean.Cidade;
import model.bean.Endereco;
import model.bean.Pessoa;

/**
 *
 * @author Maurício
 */
public class PessoaDAO  {

    private static int lastid;

    public static int getLastid() {
        return lastid;
    }

    Connection con = ConnectionFactory.getConnection();

    public boolean inserir(Object obj) {
        Pessoa p = (Pessoa) obj;

        PreparedStatement stmt = null;
        String sql = "INSERT INTO pessoa(cpf, rg, nome, sobrenome, estadocivil, religiao, telefone, email, escolaridade, idEndereco) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {
            stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, p.getCpf());
            stmt.setString(2, p.getRg());
            stmt.setString(3, p.getNome());
            stmt.setString(4, p.getSobrenome());
            stmt.setString(5, p.getEstadoCivil());
            stmt.setString(6, p.getReligiao());
            stmt.setString(7, p.getTelefone());
            stmt.setString(8, p.getEmail());
            stmt.setString(9, p.getEscolaridade());
            stmt.setInt(10, p.getIdEndereco());
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();

            if (rs.next()) {
                lastid = rs.getInt(1);
            }

        } catch (SQLIntegrityConstraintViolationException e) {
            JOptionPane.showMessageDialog(null, "Pessoa já existente!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao tentar gravar dados de pessoa");
            return false;
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
        return true;
    }

    public Pessoa busca(String cpf) {

        String sql = "SELECT * "
                + "FROM pessoa p JOIN endereco e"
                + " ON p.idEndereco = e.idEndereco "
                + " JOIN cidade c "
                + " ON e.idCidade = c.idCidade "
                + "WHERE cpf = '" + cpf + "'";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Pessoa p = new Pessoa();

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            rs.next();
            //Tratando o ResultSet recebido
            Cidade c = new Cidade();
            c.setNome(rs.getString("nomeCid"));
            c.setUf(rs.getString("uf"));
            c.setIdCidade(rs.getInt("idCidade"));
            Endereco en = new Endereco();
            en.setBairro(rs.getString("bairro"));
            en.setRua(rs.getString("rua"));
            en.setIdEndereco(rs.getInt("idEndereco"));
            en.setNumero(rs.getString("numero"));
            en.setCidade(c);
            p.setCpf(rs.getString("cpf"));
            p.setRg(rs.getString("rg"));
            p.setNome(rs.getString("nome"));
            p.setSobrenome(rs.getString("sobrenome"));
            p.setEstadoCivil(rs.getString("estadocivil"));
            p.setReligiao(rs.getString("religiao"));
            p.setTelefone(rs.getString("telefone"));
            p.setEmail(rs.getString("email"));
            p.setEscolaridade(rs.getString("escolaridade"));
            p.setIdPessoa(rs.getInt("idPessoa"));
            p.setEndereco(en);
        } catch (SQLException ex) {
            if (!cpf.equals("")) {
                JOptionPane.showMessageDialog(null, "Pessoa não encontrada!");
            } else {
                JOptionPane.showMessageDialog(null, "Informe um cpf para ser buscado!");
            }
            p = null;
        }
        return p;
    }

    public void update(Pessoa p) {
        PreparedStatement stmt = null;

        //Setando dados da pessoa
        String sql = "UPDATE pessoa SET estadocivil = ?, escolaridade = ?, telefone = ?, email = ?, religiao = ? WHERE idPessoa = " + p.getIdPessoa();

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, p.getEstadoCivil());
            stmt.setString(2, p.getEscolaridade());
            stmt.setString(3, p.getTelefone());
            stmt.setString(4, p.getEmail());
            stmt.setString(5, p.getReligiao());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao tentar gravar dados de pessoa");
        }

        //Setando dados do endereço
        sql = "UPDATE endereco SET bairro = ?, rua = ?, numero = ? WHERE idEndereco = " + p.getEndereco().getIdEndereco();

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, p.getEndereco().getBairro());
            stmt.setString(2, p.getEndereco().getRua());
            stmt.setString(3, p.getEndereco().getNumero());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao tentar gravar dados de pessoa");
        }

        //Sentando dados da cidade
        sql = "UPDATE cidade SET nomeCid = ?, uf = ? WHERE idCidade = " + p.getEndereco().getCidade().getIdCidade();

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, p.getEndereco().getCidade().getNome());
            stmt.setString(2, p.getEndereco().getCidade().getUf());
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Dados gravados.");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao tentar gravar dados de pessoa");
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }

    }
    
    public static void delete(){
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        String sql = "DELETE FROM endereco WHERE idEndereco = " + EnderecoDAO.getLastid() ;
        
        try {
            stmt = con.prepareStatement(sql);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao apagar dados de endereço");
        }
        
    }

}
