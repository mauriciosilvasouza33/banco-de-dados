/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.bean.Predio;
import model.bean.Setor;
import trabalhobanco.presenter.MudarPredioDeSetorPresenter;

/**
 *
 * @author Maurício
 */
public class PredioDAO {

    public static boolean inserir(Predio pre) {
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        boolean key = false;
        String sql = "INSERT INTO predio(nome, idSetor) values (?, ?)";

        try {

            stmt = con.prepareStatement(sql);

            if (pre.getNome().equals("")) {
                throw new NullPointerException();
            }

            //Setando os valores
            stmt.setString(1, pre.getNome());
            stmt.setInt(2, pre.getIdSetor());
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso!");
            key = true;
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Insira um nome para predio!");
        } catch (SQLIntegrityConstraintViolationException e) {
            JOptionPane.showMessageDialog(null, "Prédio já existente!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de SQL!");
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
        return key;
    }

    public static ArrayList<Predio> busca() {
        Connection con = ConnectionFactory.getConnection();
        ArrayList<Predio> predios = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM predio ORDER BY idSetor";
        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Predio pre = new Predio();
                pre.setId(rs.getInt("idPredio"));
                pre.setNome(rs.getString("nome"));
                pre.setIdSetor(rs.getInt("idSetor"));
                predios.add(pre);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de SQL");
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
        return predios;
    }

    public static ArrayList<Predio> buscaPrediosDoSetor(int id) {
        Connection con = ConnectionFactory.getConnection();
        ArrayList<Predio> predios = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM predio WHERE idSetor='" + id + "'";
        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            if (!rs.isBeforeFirst()) {
                throw new NullPointerException();
            }
            while (rs.next()) {
                Predio pre = new Predio();
                pre.setId(rs.getInt("idPredio"));
                pre.setNome(rs.getString("nome"));
                pre.setIdSetor(rs.getInt("idSetor"));
                predios.add(pre);

            }
        } catch (NullPointerException ex) {
            Predio p = new Predio();
            p.setId(0);
            p.setNome("Esse setor não possui prédios!");
            predios.add(p);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de SQL");
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
        return predios;
    }

    public static void updateNome(String predioNovo, Predio predioVelho) {
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        String sql = "UPDATE predio SET nome = '" + predioNovo + "' WHERE nome = '" + predioVelho.getNome() + "'";
        try {
            if (predioNovo.equals("")) {
                throw new NullPointerException();
            }
            if(predioNovo.equals(predioVelho.getNome())){
                throw new ArrayIndexOutOfBoundsException();
            }
            stmt = con.prepareStatement(sql);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Nome mudado com sucesso!");
        }catch(ArrayIndexOutOfBoundsException ex){
            JOptionPane.showMessageDialog(null, "O prédio já está com esse nome!");
        }catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Digite um nome para prédio!");
        } catch (SQLIntegrityConstraintViolationException ex) {
            JOptionPane.showMessageDialog(null, "Prédio ja existente!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de SQL!");
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }

    }

    public static Predio buscaPredioPorNome(Predio pre) {
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM predio WHERE nome='" + pre.getNome() + "'";
        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            rs.next();
            pre.setId(rs.getInt("idPredio"));
            pre.setIdSetor(rs.getInt("idSetor"));
            pre.setNome(rs.getString("nome"));
        } catch (SQLException ex) {
            Logger.getLogger(PredioDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
        return pre;
    }

    public static void updateIDSETOR(Predio pre, Setor set) {

        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        String sql = "UPDATE predio SET idSetor = " + set.getIdSetor() + " WHERE nome='" + pre.getNome() + "'";
        try {
            stmt = con.prepareStatement(sql);
            stmt.executeUpdate();
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Este prédio já está neste setor");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de SQL");
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
    }
    
    public static void delete(String predio){
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        String sql = "DELETE FROM predio WHERE nome = '" + predio + "'";
        try {
            stmt = con.prepareStatement(sql);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PredioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    public static String buscaSetordoPredio(String predio){
        Predio pre = new Predio();
        String setor = null;
        pre.setNome(predio);
        pre = PredioDAO.buscaPredioPorNome(pre);
        ResultSet rs = null;
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        String sql = "SELECT nome FROM setor WHERE idSetor = " + pre.getIdSetor();
        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            rs.next();
            setor = rs.getString("nome");
        } catch (SQLException ex) {
            Logger.getLogger(PredioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }  
        return setor;
    }
    
}
