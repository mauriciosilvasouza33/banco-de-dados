/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.bean.BuscaDajTable;
import trabalhobanco.presenter.CadastrarNovoTurnoPresenter;
import trabalhobanco.presenter.ManterTurnoPresenter;
import trabalhobanco.views.ManterTurnoView;

/**
 *
 * @author Maurício
 */
public class RondaDAO {

//    public int buscarIdVigilante(String cpf) {
//
//        Connection con = ConnectionFactory.getConnection();
//        String sql = "SELECT * "
//                + "FROM turno "
//                + "WHERE cpf = '" + cpf + "'";
//
//        PreparedStatement stmt;
//        ResultSet rs;
//        int i = 0;
//
//        try {
//            stmt = con.prepareStatement(sql);
//            rs = stmt.executeQuery();
//            rs.next();
//            //Tratando o ResultSet recebido
//            i = rs.getInt("idPessoa");
//        } catch (SQLException ex) {
//            if (!cpf.equals("")) {
//                JOptionPane.showMessageDialog(null, "O CPF informado não é válido");
//            } else {
//                JOptionPane.showMessageDialog(null, "Informe um cpf para ser cadastrado um turno");
//            }
//        }
//        return i;
//    }
    //Buscando id do setor para o método inserir
    public static int busca(String setor) {
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        ResultSet rs;
        String sql = "SELECT idSetor FROM setor WHERE nome = '" + setor + "'";
        int id = 0;

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            rs.next();

            id = rs.getInt("idSetor");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Setor não encontrado");
        }
        return id;
    }

    //Inserindo dados em ronda
    public void inserir(String setor, int idVigilante) throws MySQLIntegrityConstraintViolationException, SQLException {

        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        String sql = "INSERT INTO ronda(idVigilante, data, idSetor) values (?, ?, ?)";

        int id = busca(setor);

        Date data = new Date();
        SimpleDateFormat formatador = new SimpleDateFormat("yyyy-MM-dd");

        if (id != 0) {

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, idVigilante);
            stmt.setString(2, formatador.format(data));
            stmt.setInt(3, id);
            stmt.executeUpdate();

        } else {
            JOptionPane.showMessageDialog(null, "Selecione pelo menos um setor");
        }
    }

    //Buscando dados para preencher a tabela de ManterTurnoView
    public static List<BuscaDajTable> buscajtable() {
        Connection con = ConnectionFactory.getConnection();

        List<BuscaDajTable> lista = new ArrayList<>();
        PreparedStatement stmt;
        ResultSet rs;
        String sql = "select v.idVigilante, p.nome as vigilante, t.data, t.periodo, s.nome as setor, s.idSetor from pessoa p join vigilante v on p.idPessoa = v.idVigilante join turno t on v.idVigilante = t.idVigilante join ronda r on v.idVigilante = r.idVigilante and t.data = r.data join setor s on r.idSetor = s.idSetor order by t.data, t.periodo";

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {
                BuscaDajTable b = new BuscaDajTable();

                b.setIdVigilante(rs.getInt("idVigilante"));
                b.setNome(rs.getString("vigilante"));
                b.setData(rs.getString("data"));
                b.setPeriodo(rs.getString("periodo"));
                b.setSetor(rs.getString("setor"));
                b.setIdSetor(rs.getInt("idSetor"));

                lista.add(b);

            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ops! falha ao inserir dados na tabela");
        }
        return lista;
    }

    //Método para deletar ronda selecionada
    public static void deletaRonda() {

        //Buscando dados da linha selecionada na tabela
        int i = ManterTurnoPresenter.getView().getjTabelaTurno().getSelectedRow();
        List<BuscaDajTable> b = RondaDAO.buscajtable();

        Connection con = ConnectionFactory.getConnection();

        PreparedStatement stmt;
        String sql = "DELETE FROM ronda WHERE data = '" + b.get(i).getData() + "' and idVigilante = " + b.get(i).getIdVigilante() + " and idSetor = " + b.get(i).getIdSetor();

        try {
            stmt = con.prepareStatement(sql);

            //Analisando se existe ocorrencia relacionada a ronda deletando-as caso houver
            if (OcorrenciaDAO.analisaOcorrencias(b.get(i).getData(), b.get(i).getIdVigilante(), b.get(i).getIdSetor())) {
                String messagem = "A ronda selecionada possui ocorrências cadastradas, se exluí-la irá apagar suas respectivas ocorrências, deseja continuar?";
                String title = "Confirmação";
                int reply = JOptionPane.showConfirmDialog(null, messagem, title, JOptionPane.YES_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    OcorrenciaDAO.deletaOcorrencia(b.get(i).getData(), b.get(i).getIdVigilante(), b.get(i).getIdSetor());
                } else {
                    throw new NullPointerException();
                }

            }
            stmt.executeUpdate();

            //Atualizando a tabela com os turnos cadastrados
            ManterTurnoView axala = ManterTurnoPresenter.getView();
            CadastrarNovoTurnoPresenter.PreencherJTable(axala.getjTabelaTurno(), axala);

            //Analisando se ainda existe rondas naquele turno
            if (!analisaRondas(b.get(i).getData(), b.get(i).getIdVigilante(), b.get(i).getPeriodo())) {
                //Apagando turno caso não exista mais ronda para este mesmo turno
                TurnoDAO.deletaTurno(b.get(i).getData(), b.get(i).getPeriodo(), b.get(i).getIdVigilante());
            }

            JOptionPane.showMessageDialog(null, "Ronda apagada com sucesso");
        } catch (NullPointerException ne) {
            JOptionPane.showMessageDialog(null, "Ronda não apagada");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao apagar ronda");
        }
    }

    public static boolean analisaRondas(String data, int idVigilante, String periodo) {

        //Buscando todos os dados da tabela
        List<BuscaDajTable> tudo = RondaDAO.buscajtable();

        //Analisando se ainda existe ronda na tabela
        for (BuscaDajTable k : tudo) {
            if ((k.getData() == null ? data == null : k.getData().equals(data)) && k.getIdVigilante() == idVigilante && (k.getPeriodo() == null ? periodo == null : k.getPeriodo().equals(periodo))) {
                return true;
            }
        }
        return false;
    }

}
