package model.dao;

import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.bean.Setor;

public class SetorDAO {

    public static Setor inserir(Setor set) {
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        String sql = "INSERT INTO setor(nome) values (?)";

        try {
            //Preparando statement e retornando a ultima chave primaria inserida
            stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            if (set.getNome().equals("")) {
                throw new NullPointerException();
            }

            //Setando os valores
            stmt.setString(1, set.getNome());

            //Execuntando comando de inserção no banco
            stmt.executeUpdate();

            //Pegando as chaves
            ResultSet rs = stmt.getGeneratedKeys();

            //Pegando a ultima chave inserida
            if (rs.next()) {
                set.setIdSetor(rs.getInt(1));
            }
            JOptionPane.showMessageDialog(null, "Setor cadastrado com sucesso!");
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Insira um nome pra setor!");
        } catch (SQLIntegrityConstraintViolationException e) {
            JOptionPane.showMessageDialog(null, "Setor já existente!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de SQL");
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
        return set;
    }

    public static ArrayList<Setor> busca() {
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        ArrayList<Setor> setores = new ArrayList<>();
        ResultSet rs = null;
        String sql = "SELECT * FROM setor";

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Setor set = new Setor();
                set.setIdSetor(rs.getInt("idSetor"));
                set.setNome(rs.getString("nome"));
                setores.add(set);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de SQL");
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
        return setores;
    }

    public static Setor returnSet(int id) {
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs;
        Setor set = new Setor();
        String sql = "SELECT * FROM setor WHERE idSetor = " + id;
        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            rs.next();
            set.setNome(rs.getString("nome"));
            set.setIdSetor(id);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de SQL");
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }

        return set;
    }

    public static ArrayList<Setor> buscaSetoresComPredios() {
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt= null;
        ArrayList<Setor> setores = new ArrayList<>();
        ResultSet rs;
        String sql = "SELECT DISTINCT setor.idSetor, setor.nome FROM setor inner join predio on setor.idSetor = predio.idSetor ";

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Setor set = new Setor();
                set.setIdSetor(rs.getInt("idSetor"));
                set.setNome(rs.getString("nome"));
                setores.add(set);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de SQL");
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
        return setores;
    }
    
    public static boolean updateNomeSetor(String novoNome, String nomeVelho){
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt= null;
        boolean updated = false;
        String sql = "UPDATE setor set nome = '" + novoNome +"' WHERE nome = '" + nomeVelho + "'"; 
         try {
            if(novoNome.equals("")){
            }
            stmt = con.prepareStatement(sql);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Nome do setor mudado com sucesso!");
            updated = true;
        }catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Digite um nome para o novo setor!");
        } catch (SQLIntegrityConstraintViolationException e) {
            JOptionPane.showMessageDialog(null, "Já existe setor com esse nome!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de SQL!");
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
         return updated;
    }
    
     public static void delete(String setor){
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        String sql = "DELETE setor.*, predio.* FROM setor inner join predio on predio.idSetor = setor.idSetor WHERE setor.nome = '" + setor + "'";
        try {
            stmt = con.prepareStatement(sql);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de SQL!");
        }        
    }
    
}
