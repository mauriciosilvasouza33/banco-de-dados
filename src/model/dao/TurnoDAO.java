/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Maurício
 */
public class TurnoDAO {

    //Buscando id do vigilante em pessoa
    public int buscarIdVigilante(String cpf) {

        Connection con = ConnectionFactory.getConnection();
        String sql = "SELECT idPessoa "
                + "FROM pessoa "
                + "WHERE cpf = '" + cpf + "'";

        PreparedStatement stmt;
        ResultSet rs;
        int i = 0;

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            rs.next();
            //Tratando o ResultSet recebido
            i = rs.getInt("idPessoa");
        } catch (SQLException ex) {
            if (!cpf.equals("")) {
                JOptionPane.showMessageDialog(null, "O CPF informado não é válido");
            } else {
                JOptionPane.showMessageDialog(null, "Informe um cpf para ser cadastrado em um turno");
            }
        }
        return i;
    }

    //Inserindo dados em turno
    public void inserir(int idVigilante, String pe) throws SQLIntegrityConstraintViolationException, SQLException {

        Connection con = ConnectionFactory.getConnection();
        Date data = new Date();
        SimpleDateFormat formatador = new SimpleDateFormat("yyyy-MM-dd");
        PreparedStatement stmt = null;
        String sql = "INSERT INTO turno(idVigilante, data, periodo) values (?, ?, ?)";

        stmt = con.prepareStatement(sql);

        stmt.setInt(1, idVigilante);
        stmt.setString(2, formatador.format(data));
        stmt.setString(3, pe);

        stmt.executeUpdate();

        ConnectionFactory.closeConnection(con);

    }

    public static void deletaTurno(String data, String periodo, int idVigilante) throws SQLException {

        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;

        String sql = "DELETE FROM turno WHERE data = '" + data + "' and idVigilante = " + idVigilante + " and periodo = '" + periodo + "'";
        stmt = con.prepareStatement(sql);
        stmt.executeUpdate();
        
    }
    
    public static boolean busca(String periodo, String data){
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        boolean exist = false;
        String sql = "SELECT periodo FROM turno WHERE periodo = '" + periodo + "' AND data = '" + data + "'";
        ResultSet rs;
        System.out.println(sql);
        
        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            
           exist = rs.next();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao buscar por periodo");
        }
        return exist;
    }

}
