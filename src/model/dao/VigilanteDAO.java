/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Maurício
 */
public class VigilanteDAO {

    private int lastid;

    public int getLastid() {
        return lastid;
    }

    Connection con = ConnectionFactory.getConnection();

    public void inserir(int idPessoa) {

        PreparedStatement stmt = null;
        String sql = "INSERT INTO vigilante(idVigilante) values (?)";

        try {
            stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stmt.setInt(1, idPessoa);

            stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();

            if (rs.next()) {
                lastid = rs.getInt(1);
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possível inserir dados em vigilante");
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }

    }

    public static void demite(int idVigilante) throws SQLException{

        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        String sql = "UPDATE vigilante SET ativo = 'n' WHERE idVigilante = " + idVigilante;

            stmt = con.prepareStatement(sql);
            stmt.executeUpdate();

    }

    public static void readminte(int idVigilante) {

        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        String sql = "UPDATE vigilante SET ativo = 's' WHERE idVigilante = " + idVigilante;

        try {
            stmt = con.prepareStatement(sql);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao readmitir vigilante");
        }
    }

    public static String busca(int idVigilante) {

        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        ResultSet rs;
        String sql = "SELECT * FROM vigilante WHERE idVigilante = " + idVigilante;
        String p = "a";
        
        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            rs.next();
            p = rs.getString("ativo");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao buscar vigilante");
        }
        return p;
    }
    
    public static void delete(){
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt;
        String sql = "DELETE FROM vigilante WHERE idVigilante = " + PessoaDAO.getLastid() ;
        
        try {
            stmt = con.prepareStatement(sql);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao apagar dados de vigilante");
        }
        
    }

}
