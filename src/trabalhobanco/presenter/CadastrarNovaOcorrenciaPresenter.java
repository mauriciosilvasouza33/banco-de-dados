/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhobanco.presenter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.bean.BuscaDajTable;
import model.bean.Ocorrencia;
import model.bean.Predio;
import model.dao.OcorrenciaDAO;
import model.dao.PredioDAO;
import model.dao.RondaDAO;
import trabalhobanco.views.CadastrarNovaOcorrenciaView;
import trabalhobanco.views.ManterTurnoView;

/**
 *
 * @author Maurício
 */
public class CadastrarNovaOcorrenciaPresenter {

    private static CadastrarNovaOcorrenciaView view;
    private static CadastrarNovaOcorrenciaPresenter instance;

    private CadastrarNovaOcorrenciaPresenter() {

        view = new CadastrarNovaOcorrenciaView();
        this.iniciarTela();

    }

    private void iniciarTela() {
        //Implementando botão de exit do JFrame
        view.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                ManterTurnoPresenter.getInstance();
                
                //REMOVENDO INTENS DO JCOMBOBOX DE PRÉDIOS
                view.getjComboBoxPredio().removeAllItems();
                //Atualizando a tabela de ocorrências
                ManterTurnoView axala = ManterTurnoPresenter.getView();
                OcorrenciaDAO.PreencherJTable(axala.getjTabelaOcorrencias(), axala);
                
                axala.getBtnDelRonda().setEnabled(false);
                axala.getBtnNovaOcorrencia().setEnabled(false);
                axala.getBtnOcoEdit().setEnabled(false);
                instance = null;
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });

        //Implementando botão de voltar
        view.getBtnVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                ManterTurnoPresenter.getInstance();
                view.setVisible(false);
                view.dispose();

                //REMOVENDO INTENS DO JCOMBOBOX DE PRÉDIOS
                view.getjComboBoxPredio().removeAllItems();
                
                //Atualizando a tabela de ocorrências
                ManterTurnoView axala = ManterTurnoPresenter.getView();
                OcorrenciaDAO.PreencherJTable(axala.getjTabelaOcorrencias(), axala);
                
                axala.getBtnDelRonda().setEnabled(false);
                axala.getBtnNovaOcorrencia().setEnabled(false);
                instance = null;

            }
        });

        //Implementando botão de cadastrar nova ocorrência
        view.getBtnCadastrar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Ocorrencia o = new Ocorrencia();
                try {
                    if (view.getTxtDescricao().getText().equals("")) {
                        throw new NullPointerException();
                    }

                    //Buscando dados do item selecionado na tabela registros de turnos
                    int i = ManterTurnoPresenter.getView().getjTabelaTurno().getSelectedRow();
                    List<BuscaDajTable> b = RondaDAO.buscajtable();
                    CadastrarNovaOcorrenciaView v = CadastrarNovaOcorrenciaPresenter.getView();
                    o.setIdSetor(b.get(i).getIdSetor());

                    //BUSCANDO O PRÉDIO RELACIONADO AO SETOR SELECIONADO
                    ArrayList<Predio> l;
                    l = PredioDAO.buscaPrediosDoSetor(b.get(i).getIdSetor());

                    for (Predio p : l) {
                        if (p.getNome().equals(view.getjComboBoxPredio().getSelectedItem().toString())) {
                            o.setIdPredio(p.getId());
                            break;
                        }
                    }

                    //Pegando data, idVigilante e descrição
                    o.setData(b.get(i).getData());
                    o.setIdVigilante(b.get(i).getIdVigilante());
                    o.setDescricao(view.getTxtDescricao().getText());

                    OcorrenciaDAO.inserir(o);
                    JOptionPane.showMessageDialog(null, "Ocorrência cadastrada com sucesso");

                }catch(NullPointerException ne){
                    JOptionPane.showMessageDialog(null, "Informe a descrição da ocorrência");
                }

            }
        });
    }

    public static CadastrarNovaOcorrenciaPresenter getInstance() {
        if (instance == null) {
            instance = new CadastrarNovaOcorrenciaPresenter();
        }
        view.setVisible(true);
        return instance;
    }

    public static CadastrarNovaOcorrenciaView getView() {
        return view;
    }

}
