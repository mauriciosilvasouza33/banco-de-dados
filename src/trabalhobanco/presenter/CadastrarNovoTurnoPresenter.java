/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhobanco.presenter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.bean.BuscaDajTable;
import model.dao.RondaDAO;
import model.dao.TurnoDAO;
import trabalhobanco.views.CadastrarNovoTurnoView;
import trabalhobanco.views.ManterTurnoView;

/**
 *
 * @author Maurício
 */
public class CadastrarNovoTurnoPresenter {

    private static CadastrarNovoTurnoView view;
    private static CadastrarNovoTurnoPresenter instance;

    private CadastrarNovoTurnoPresenter() {

        view = new CadastrarNovoTurnoView();
        this.iniciarTela();

    }

    public static CadastrarNovoTurnoPresenter getInstance() {
        if (instance == null) {
            instance = new CadastrarNovoTurnoPresenter();
        }
        view.setVisible(true);
        return instance;
    }

    private void iniciarTela() {

        //Implementando botão de salvar
        view.getBtnVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ManterTurnoPresenter.getInstance();

                //Atualizando a tabela com os turnos cadastrados
                ManterTurnoView axala = ManterTurnoPresenter.getView();
                PreencherJTable(axala.getjTabelaTurno(), axala);
                axala.getBtnDelRonda().setEnabled(false);
                axala.getBtnNovaOcorrencia().setEnabled(false);
                axala.getBtnOcoEdit().setEnabled(false);
                instance = null;

                view.setVisible(false);
                view.dispose();
            }
        });

        //Salvando dados de um novo turno
        view.getBtnSalvar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                TurnoDAO tdao = new TurnoDAO();
                int idVigilante = tdao.buscarIdVigilante(view.getTxtCPF().getText());
                Date data = new Date();
                SimpleDateFormat formatador = new SimpleDateFormat("yyyy-MM-dd");
                if (!TurnoDAO.busca(view.getjComboBoxPeriodo().getSelectedItem().toString(), formatador.format(data))) {

                    if (idVigilante != 0) {
                        try {

                            //pegando valores do jtable
                            RondaDAO rdao = new RondaDAO();
                            int[] ids = view.getjTableSetores().getSelectedRows();
                            if (ids.length == 0) {
                                throw new NullPointerException();
                            }

                            //inserindo ronda
                            for (int i = 0; i < ids.length; i++) {
                                rdao.inserir(view.getjTableSetores().getValueAt(ids[i], 0).toString(), idVigilante);
                            }

                            //inserindo turno
                            tdao.inserir(idVigilante, (String) view.getjComboBoxPeriodo().getSelectedItem());

                            JOptionPane.showMessageDialog(null, "Turno cadastrado com sucesso");
                        } catch (NullPointerException ne) {
                            JOptionPane.showMessageDialog(null, "Por favor, informe ao menos um setor");
                        } catch (SQLIntegrityConstraintViolationException si) {
                            JOptionPane.showMessageDialog(null, "Já existe um vigilante cadastrado neste turno ou vigilante ja trabalhou em um turno anterior");
                        } catch (SQLException ex) {
                            JOptionPane.showMessageDialog(null, "Erro ao inserir dados do vigilante");
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Já existe um vigilante trabalhando no periodo selecionado");
                }
            }
        });

        //Implementando botão de exit do JFrame
        view.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                ManterTurnoPresenter.getInstance();
                instance = null;
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });

    }

    //Pegando view para atualizar horas
    public static CadastrarNovoTurnoView getView() {
        return view;
    }

    //Método para preencher jTable de turnos
    public static void PreencherJTable(JTable jt, ManterTurnoView view) {
        List<BuscaDajTable> b = RondaDAO.buscajtable();
        DefaultTableModel modelo = (DefaultTableModel) jt.getModel();
        modelo.setNumRows(0);

        for (BuscaDajTable i : b) {
            modelo.addRow(new Object[]{
                i.getNome(), i.getData(), i.getPeriodo(), i.getSetor()
            });
        }
    }
}
