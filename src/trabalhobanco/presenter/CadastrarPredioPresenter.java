/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhobanco.presenter;

import Supporte.Preenchedor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import model.bean.Predio;
import model.bean.Setor;
import model.dao.PredioDAO;
import model.dao.SetorDAO;
import trabalhobanco.views.CadastrarPredioView;

/**
 *
 * @author Gabriel
 */
public class CadastrarPredioPresenter {

    private static CadastrarPredioView view;
    private static CadastrarPredioPresenter instance;

    private CadastrarPredioPresenter() {
        view = new CadastrarPredioView();
        this.iniciarTela();
    }

    public static CadastrarPredioPresenter getInstance() {
        if (instance == null) {
            instance = new CadastrarPredioPresenter();
        }
        view.setVisible(true);
        return instance;
    }

    private void iniciarTela() {
        //Implementando botão de voltar
        view.getBtnVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ManterSetoresEPrediosPresenter.getInstance();
                view.setVisible(false);
                view.dispose();
            }
        });

        //Implementando botão de castrar prédio
        view.getBtnSalvar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                //Criando Setor
                Setor setr = new Setor();
                setr.setNome(view.getCbSetores().getSelectedItem().toString());

                //Buscando setor que quero
                for (Setor set : SetorDAO.busca()) {
                    if (set.getNome().equals(setr.getNome())) {
                        setr.setIdSetor(set.getIdSetor());
                        break;
                    }
                }

                //Criando Predio
                Predio pre = new Predio();
                pre.setNome(view.getTxtNovoPredio().getText());
                pre.setIdSetor(setr.getIdSetor());

                //Inserindo Predio
                if (PredioDAO.inserir(pre)) {
                    
                    //Atualizando JComboBoxPredios
                    view.getCbPredios().removeAllItems();
                    ArrayList<Predio> predios = PredioDAO.buscaPrediosDoSetor(setr.getIdSetor());
                    for (Predio p : predios) {
                        view.getCbPredios().addItem(p.getNome());
                    }
                    view.getCbPredios().setSelectedItem(view.getTxtNovoPredio().getText());
                }

            }
        });

        //Implementando click no JComboBox
        view.getCbSetores().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        view.getCbPredios().removeAllItems();
                        //Procura todos os setores
                        ArrayList<Setor> setores = SetorDAO.busca();
                        Setor set = new Setor();

                        //Verifica o nome de cada setor, até achar o nome do que foi selecionado no jCombo
                        for (Setor s : setores) {
                            if (view.getCbSetores().getSelectedItem() == null) {
                                break;
                            }
                            if (s.getNome().equals(view.getCbSetores().getSelectedItem().toString())) {
                                set = s;
                                break;
                            }
                        }

                        //Populando o jComboBoxPredios
                        ArrayList<Predio> predios = PredioDAO.buscaPrediosDoSetor(set.getIdSetor());
                        for (Predio p : predios) {
                            view.getCbPredios().addItem(p.getNome());
                        }
                    }
                }
                );

        //Implementando botão de exit do JFrame
        view.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
                //Limpando comboBoxes
                view.getCbSetores().removeAllItems();
                view.getCbPredios().removeAllItems();

                //Populando o jComboBoxSetor
                Preenchedor.PreencherJComboBoxSetor(CadastrarPredioPresenter.getView().getCbSetores());

            }

            @Override
            public void windowClosing(WindowEvent e) {
                ManterSetoresEPrediosPresenter.getInstance();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                instance = null;
            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
    }

    public static CadastrarPredioView getView() {
        return view;
    }

}
