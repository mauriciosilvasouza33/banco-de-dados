package trabalhobanco.presenter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import model.bean.Cidade;
import model.bean.Endereco;
import model.bean.Pessoa;
import model.dao.CidadeDAO;
import model.dao.ContratoDAO;
import model.dao.EnderecoDAO;
import model.dao.PessoaDAO;
import model.dao.VigilanteDAO;
import trabalhobanco.views.TelaCadastrarView;

public class CadastrarPresenter {

    private static TelaCadastrarView view;
    private static CadastrarPresenter instance;

    private CadastrarPresenter() {
        view = new TelaCadastrarView();
        this.iniciarTela();
    }

    private void iniciarTela() {

        view.getBtnVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PrincipalPresenter.getInstance();
                view.setVisible(false);
                view.dispose();
            }
        });

        view.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                view.setVisible(false);
                view.dispose();
                PrincipalPresenter.getInstance();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                instance = null;
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });

        //Botão de salvar
        view.getBtnSalvar().addActionListener(new ActionListener() {
            @Override

            public void actionPerformed(ActionEvent e) {

                Cidade ci = new Cidade();
                CidadeDAO cidao = new CidadeDAO();
                ci.setNome(view.getTextCid().getText());
                ci.setUf(view.getTextEstado().getText());

                // Inserindo cidade
                if (cidao.inserir(ci)) {
                    Endereco end = new Endereco();
                    EnderecoDAO enddao = new EnderecoDAO();
                    end.setBairro(view.getTextBairro().getText());
                    end.setRua(view.getTextRua().getText());
                    end.setNumero(view.getTextNum().getText());
                    end.setIdCidade(CidadeDAO.getLastid());

                    // Inserindo endereço
                    if (enddao.inserir(end)) {
                        Pessoa p = new Pessoa();
                        PessoaDAO pdao = new PessoaDAO();
                        p.setCpf(view.getTextCPF().getText());
                        p.setEmail(view.getTextemail().getText());
                        p.setNome(view.getTextNome().getText());
                        p.setEstadoCivil(view.getTextEstadoCivil().getText());
                        p.setReligiao(view.getTextReli().getText());
                        p.setSobrenome(view.getTextSobrenome().getText());
                        p.setRg(view.getTextRG().getText());
                        p.setTelefone(view.getTextTel().getText());
                        p.setEscolaridade(view.getTextEscola().getText());
                        p.setIdEndereco(EnderecoDAO.getLastid());

                        //Inserindo pessoa
                        if (pdao.inserir(p)) {

                            //Inserindo vigilante
                            VigilanteDAO vidao = new VigilanteDAO();
                            vidao.inserir(PessoaDAO.getLastid());

                            //Inserindo contrato do vigilante
                            ContratoDAO codao = new ContratoDAO();
                            try {
                                double i = Double.parseDouble(view.getTextSal().getText());
                                if (codao.inserir(i, PessoaDAO.getLastid())) {
                                    JOptionPane.showMessageDialog(null, "Dados cadastrados com sucesso");
                                    view.getBtnSalvar().setEnabled(false);
                                } else {
                                    ContratoDAO.delete();
                                    PessoaDAO.delete();
                                    EnderecoDAO.delete();
                                    VigilanteDAO.delete();
                                    JOptionPane.showMessageDialog(null, "Salário incorreto");
                                }

                            } catch (NumberFormatException ne) {
                                ContratoDAO.delete();
                                PessoaDAO.delete();
                                EnderecoDAO.delete();
                                VigilanteDAO.delete();
                                JOptionPane.showMessageDialog(null, "Salário incorreto");
                            }
                        } else {
                            PessoaDAO.delete();
                            EnderecoDAO.delete();
                        }

                    } else {
                        EnderecoDAO.delete();
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Dados de relacionados a cidade estão incorretos");
                }

            }
        });

    }

    public static CadastrarPresenter getInstance() {
        if (instance == null) {
            instance = new CadastrarPresenter();
        }
        view.setVisible(true);
        return instance;
    }

    public TelaCadastrarView getTela() {
        return view;
    }

}
