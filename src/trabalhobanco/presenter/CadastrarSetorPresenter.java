/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhobanco.presenter;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import model.bean.Setor;
import model.dao.SetorDAO;
import trabalhobanco.views.CadastrarSetorView;

/**
 *
 * @author Maurício
 */
public class CadastrarSetorPresenter {
    private static CadastrarSetorView view;
    private static CadastrarSetorPresenter instance;
    
    private CadastrarSetorPresenter(){
        view = new CadastrarSetorView();
        this.iniciarTela();
    }
    
     public static CadastrarSetorPresenter getInstance(){
        if(instance == null){
            instance = new CadastrarSetorPresenter();
        }
        view.setVisible(true);
        return instance;
    }
     
    private void iniciarTela(){   
        
        //Implementando botão de voltar
        view.getBtnVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ManterSetoresEPrediosPresenter.getInstance();
                view.setVisible(false);
                view.dispose();
            }
        });
        
        //Implementando botão de salvar prédio
        view.getBtnSalvar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Setor set = new Setor();
                set.setNome(view.getTxtSetor().getText());
                SetorDAO.inserir(set);
                }
        });
        
        //Implementando botão de exit do JFrame
        view.addWindowListener(new WindowListener(){
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                ManterSetoresEPrediosPresenter.getInstance();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                instance = null;
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }        
        });
        
    }
}
