/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhobanco.presenter;


import Supporte.Preenchedor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import model.dao.SetorDAO;
import trabalhobanco.views.ExcluirSetorView;

/**
 *
 * @author Gabriel
 */
public class ExcluirSetorPresenter {
    private static ExcluirSetorView view;
    private static ExcluirSetorPresenter instance;
    
    private ExcluirSetorPresenter(){
        view = new ExcluirSetorView();
        this.iniciarTela();
    }
    
    private void iniciarTela(){
        //Implementando botão de exit do JFrame
        view.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
                Preenchedor.PreencherJComboBoxSetor(view.getjComboBox1());
            }

            @Override
            public void windowClosing(WindowEvent e) {
                ManterSetoresEPrediosPresenter.getInstance();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                instance = null;
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }

        });
        
         //Implementando botão de voltar
        view.getBtnVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ManterSetoresEPrediosPresenter.getInstance();
                view.setVisible(false);
                view.dispose();
            }
        });
        
        //Implementando botão de deletar
        view.getBtnSalvar().addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                SetorDAO.delete(view.getjComboBox1().getSelectedItem().toString());
                view.getjComboBox1().removeItem(view.getjComboBox1().getSelectedItem());
            }        
        });
    }
    
    public static ExcluirSetorPresenter getInstance(){
        if(instance == null){
            instance = new ExcluirSetorPresenter();
        }
        view.setVisible(true);
        return instance;
    }
    
}
