package trabalhobanco.presenter;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import model.dao.AdministradorDAO;
import trabalhobanco.views.TelaLoguinView;

public class LoginPresenter {

    private static TelaLoguinView view;
    private static LoginPresenter instance;

    private LoginPresenter() {
        view = new TelaLoguinView();
        this.iniciarTela();
    }

    public static LoginPresenter getInstance() {
        if (instance == null) {
            instance = new LoginPresenter();
        }
        view.setVisible(true);
        return instance;
    }

    private void iniciarTela() {
        view.getBtnEntrar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean text = AdministradorDAO.buscaLoginSenha(view.getTextLogin().getText(), view.getTextSenha().getText());
                if (text) {
                    PrincipalPresenter.getInstance();
                    view.setVisible(false);
                    view.dispose();
                }
            }
        });

        view.getBtnSair().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        view.getTextLogin().addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (view.getTextLogin().getText().equals("Login")) {
                    view.getTextLogin().setText("");
                    view.getTextLogin().setForeground(new Color(0, 0, 0));
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (view.getTextLogin().getText().equals("")) {
                    view.getTextLogin().setText("Login");
                    view.getTextLogin().setForeground(new Color(169 , 169, 169));
                }

            }
        });

        view.getTextSenha().addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (view.getTextSenha().getText().equals("Senha")) {
                    view.getTextSenha().setText("");
                    view.getTextSenha().setForeground(new Color(0, 0, 0));
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (view.getTextSenha().getText().equals("")) {
                    view.getTextSenha().setText("Senha");
                    view.getTextSenha().setForeground(new Color(169, 169, 169));
                }

            }
        });
    }
}
