package trabalhobanco.presenter;

import Supporte.Preenchedor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JComboBox;
import javax.swing.JTable;
import model.bean.Predio;
import model.dao.PredioDAO;
import trabalhobanco.views.ManterSetoresEPrediosView;

/*
 * @author Maurício
 */
public class ManterSetoresEPrediosPresenter {

    private static ManterSetoresEPrediosView view;
    private static ManterSetoresEPrediosPresenter instance;

    private ManterSetoresEPrediosPresenter() {
        view = new ManterSetoresEPrediosView();
        IniciarTela();
    }

    public static ManterSetoresEPrediosPresenter getInstance() {
        if (instance == null) {
            instance = new ManterSetoresEPrediosPresenter();
        }
        view.setVisible(true);
        return instance;
    }

    private void IniciarTela() {
        //Implementando botão de voltar
        view.getBtnVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PrincipalPresenter.getInstance();
                view.setVisible(false);
                view.dispose();
            }
        });

        //Implementando botão de editar nome de setor
        view.getBtnMudarNomeSetor().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MudarNomeSetorPresenter.getInstance();
                view.setVisible(false);
                view.dispose();
            }

        });

        //Implementando botão de exit do JFrame
        view.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
                JTable tabela = view.getTabelaSetor();
                Preenchedor.PreencherJTableSetorEPredio(tabela);
                view.getBtnEditar().setEnabled(false);
                view.getBtnMudarPredio().setEnabled(false);
                view.getBtnExcluirPredio().setEnabled(false);
                if (!view.getBtnMudarPredio().isEnabled()) {
                    view.getBtnMudarPredio().setToolTipText("Selecione algum registro da tabela de setores e prédios");
                }
                if (!view.getBtnEditar().isEnabled()) {
                    view.getBtnEditar().setToolTipText("Selecione algum registro da tabela de setores e prédios");
                }
                if (!view.getBtnExcluirPredio().isEnabled()) {
                    view.getBtnExcluirPredio().setToolTipText("Selecione algum registro da tabela de setores e prédios");
                }
            }

            @Override
            public void windowClosing(WindowEvent e) {
                PrincipalPresenter.getInstance();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                instance = null;
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }

        });

        //Implementando botão de MuddarPredioDeSetor
        view.getBtnMudarPredio().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setVisible(false);
                view.dispose();
                MudarPredioDeSetorPresenter.getInstance();

                //Preenchendo o jc com todos os setores 
                JComboBox jc = MudarPredioDeSetorPresenter.getView().getjComboBoxSetor();
                jc.removeAllItems();
                Preenchedor.PreencherJComboBoxSetor(jc);

                //Pegando linha selecionada
                int linha = view.getTabelaSetor().getSelectedRow();
                jc.setSelectedItem(view.getTabelaSetor().getValueAt(linha, 0));

                //Setando o nome do predio no txtSetor
                Predio pre = new Predio();
                pre.setNome(view.getTabelaSetor().getValueAt(linha, 1).toString());
                MudarPredioDeSetorPresenter.setarPredio(PredioDAO.buscaPredioPorNome(pre));
            }

        });
        
        //Implementando botão de excluir setor
        view.getBtnExcluirSetor().addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                ExcluirSetorPresenter.getInstance();
                view.setVisible(false);
                view.dispose();
            }
            
        });

        //Implementando botão de excluir prédio
        view.getBtnExcluirPredio().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int linha = view.getTabelaSetor().getSelectedRow();
                String predio = view.getTabelaSetor().getValueAt(linha, 1).toString();
                PredioDAO.delete(predio);
                JTable tabela = view.getTabelaSetor();
                Preenchedor.PreencherJTableSetorEPredio(tabela);
            }

        });

        //Implementando botão de cadastrar Setor
        view.getBtnCadastrarSetor().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setVisible(false);
                view.dispose();
                CadastrarSetorPresenter.getInstance();
            }
        });

        //Implementando ListSelectionListener
        view.getTabelaSetor().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                view.getBtnEditar().setEnabled(true);
                view.getBtnMudarPredio().setEnabled(true);
                view.getBtnExcluirPredio().setEnabled(true);
                view.getBtnMudarPredio().setToolTipText(null);
                view.getBtnEditar().setToolTipText(null);
                view.getBtnExcluirPredio().setToolTipText(null);
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

        });

        //Implementando botão de abrir cadastrar prédio
        view.getBtnCadastrarPredio().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CadastrarPredioPresenter.getInstance();
                view.setVisible(false);
                view.dispose();
            }

        });

        //Implementando botão mudar nome do prédio
        view.getBtnEditar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setVisible(false);
                view.dispose();
                MudarNomePredioPresenter.getInstance();
                int linha = view.getTabelaSetor().getSelectedRow();
                Predio pe = new Predio();
                String setor = view.getTabelaSetor().getValueAt(linha, 0).toString();
                pe.setNome(view.getTabelaSetor().getValueAt(linha, 1).toString());
                MudarNomePredioPresenter.SetarPredioESetor(setor, pe);
            }
        });
    }

    public static ManterSetoresEPrediosView getView() {
        return view;
    }

}
