package trabalhobanco.presenter;

import Supporte.Preenchedor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.swing.JTable;
import model.bean.BuscaDajTable;
import model.bean.Ocorrencia;
import model.bean.Predio;
import model.dao.OcorrenciaDAO;
import model.dao.PessoaDAO;
import model.dao.PredioDAO;
import model.dao.RondaDAO;
import trabalhobanco.views.CadastrarNovaOcorrenciaView;
import trabalhobanco.views.CadastrarNovoTurnoView;
import trabalhobanco.views.ExibirRelatorioView;
import trabalhobanco.views.ManterTurnoView;
import trabalhobanco.views.TelaEditarOcorrenciaView;

/**
 *
 * @author Maurício
 */
public class ManterTurnoPresenter {

    private static ManterTurnoPresenter instance;
    private static ManterTurnoView view;

    private ManterTurnoPresenter() {
        view = new ManterTurnoView();
        iniciarTela();
    }

    private void iniciarTela() {

        //Implementando botão de exibirRelatorio
        view.getBtnGerarRelatorio().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ExibirRelatorioView relatorio = new ExibirRelatorioView();
                relatorio.setVisible(true);
                JTable tabela = view.getjTabelaOcorrencias();
                int linha = tabela.getSelectedRow();
                PessoaDAO pessoa = new PessoaDAO();
                Date data = new Date(System.currentTimeMillis());
                SimpleDateFormat formatarDate = new SimpleDateFormat("dd-MM-YYYY");
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                Date hora = Calendar.getInstance().getTime(); // Ou qualquer outra forma que tem
                String dataFormatada = sdf.format(hora);

                //Preenchendo relatório
                relatorio.getLblVigilante().setText(tabela.getValueAt(linha, 0).toString());
                relatorio.getLblCPF().setText(tabela.getValueAt(linha, 1).toString());
                String cpf = tabela.getValueAt(linha, 1).toString();
                Integer idPessoa = pessoa.busca(cpf).getIdPessoa();
                relatorio.getLblId().setText(idPessoa.toString());
                relatorio.getLblDataEmissao().setText(formatarDate.format(data));
                relatorio.getLblHorarioEmissao().setText(dataFormatada);
                relatorio.getLblSetor().setText(PredioDAO.buscaSetordoPredio(tabela.getValueAt(linha, 4).toString()));
                relatorio.getLblPredio().setText(tabela.getValueAt(linha,4).toString());
                relatorio.getLblDataOcorrencia().setText(tabela.getValueAt(linha, 2).toString());
                relatorio.getLblPeriodo().setText(tabela.getValueAt(linha,4).toString());
                relatorio.getLblDescr().setText(tabela.getValueAt(linha, 5).toString());

            }

        });

        //Implementando botão de voltar
        view.getBtnVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setVisible(false);
                view.getBtnNovaOcorrencia().setEnabled(false);
                view.getBtnOcoEdit().setEnabled(false);
                view.getBtnDelRonda().setEnabled(false);
                view.dispose();
                PrincipalPresenter.getInstance();
                instance = null;
            }
        });

        //Implementando botão de cadastrar turno
        view.getBtnTurnoCadastro().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CadastrarNovoTurnoPresenter.getInstance();
                Date data = new Date();
                CadastrarNovoTurnoView p = CadastrarNovoTurnoPresenter.getView();
                SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
                p.getTxtDia().setText(formatador.format(data));
                Preenchedor.PreencherJTableSetor(p.getjTableSetores());
                view.setVisible(false);
                view.dispose();
            }
        });

        //
        //Implementando o exit o JFrame
        view.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
                view.getBtnGerarRelatorio().setEnabled(false);
            }

            @Override
            public void windowClosing(WindowEvent e) {
                PrincipalPresenter.getInstance();
                instance = null;
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });

        //Acionando botão de cadastro de nova ocorrência
        view.getjTabelaTurno().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (view.getjTabelaTurno().getSelectedRow() != -1) {
                    view.getBtnNovaOcorrencia().setEnabled(true);
                    view.getBtnDelRonda().setEnabled(true);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        view.getBtnNovaOcorrencia().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                CadastrarNovaOcorrenciaPresenter.getInstance();
                view.setVisible(false);
                view.dispose();

                //Buscando dados do item selecionado na tabela registros de turnos
                int i = ManterTurnoPresenter.getView().getjTabelaTurno().getSelectedRow();
                List<BuscaDajTable> b = RondaDAO.buscajtable();
                CadastrarNovaOcorrenciaView v = CadastrarNovaOcorrenciaPresenter.getView();
                v.getTxtTurno().setText(b.get(i).getPeriodo());
                v.getTxtSetor().setText(b.get(i).getSetor());
                v.getTxtVigilante().setText(b.get(i).getNome());

                //BUSCANDO OS PRÉDIOS RELACIONADOS AOS SETORES
                ArrayList<Predio> l = new ArrayList<>();
                l = PredioDAO.buscaPrediosDoSetor(b.get(i).getIdSetor());

                //MOSTRANDO PRÉDIOS DO RESPECTIVO SETOR SELECIONADO
                for (Predio p : l) {
                    v.getjComboBoxPredio().addItem(p.getNome());
                }
            }
        });

        view.getBtnOcoEdit().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                TelaEditarOcorrenciaPresenter.getInstance();
                view.setVisible(false);
                view.dispose();

                //Buscando dados para inserir na tela de edição
                int i = view.getjTabelaOcorrencias().getSelectedRow();
                List<Ocorrencia> o = OcorrenciaDAO.buscaOcorrencia();
                TelaEditarOcorrenciaView v = TelaEditarOcorrenciaPresenter.getView();
                v.getTxtVigilante().setText(o.get(i).getNome());
                v.getTxtCPF().setText(o.get(i).getCpf());
                v.getTxtData().setText(o.get(i).getData());
                v.getTxtPeriodo().setText(o.get(i).getPeriodo());
                v.getTxtPredio().setText(o.get(i).getPredio());
                v.getjTextDescricao().setText(o.get(i).getDescricao());
            }
        });

        view.getjTabelaOcorrencias().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (view.getjTabelaOcorrencias().getSelectedRow() != -1) {
                    view.getBtnOcoEdit().setEnabled(true);
                    view.getBtnGerarRelatorio().setEnabled(true);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        //Botão de deletar ronda
        view.getBtnDelRonda().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                RondaDAO.deletaRonda();

                //Atualizando a tabela de ocorrências
                OcorrenciaDAO.PreencherJTable(view.getjTabelaOcorrencias(), view);

                //Setando enable dos botões para false
                view.getBtnDelRonda().setEnabled(false);
                view.getBtnNovaOcorrencia().setEnabled(false);
                view.getBtnOcoEdit().setEnabled(false);

            }
        });

    }

    public static ManterTurnoPresenter getInstance() {
        if (instance == null) {
            instance = new ManterTurnoPresenter();
        }
        view.setVisible(true);
        return instance;
    }

    //Pegando view para atualizar horas
    public static ManterTurnoView getView() {
        return view;
    }

}
