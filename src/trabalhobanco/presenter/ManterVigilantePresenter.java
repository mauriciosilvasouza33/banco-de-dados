package trabalhobanco.presenter;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import model.bean.Contrato;
import model.bean.Pessoa;
import model.bean.Vigilante;
import model.dao.ContratoDAO;
import model.dao.PessoaDAO;
import model.dao.VigilanteDAO;
import trabalhobanco.views.TelaManterVigilanteView;

public class ManterVigilantePresenter {

    private static ManterVigilantePresenter instance;
    private static TelaManterVigilanteView view;
    Pessoa p = new Pessoa();

    private ManterVigilantePresenter() {
        view = new TelaManterVigilanteView();
        iniciarTela();
    }

    public static ManterVigilantePresenter getInstance() {
        if (instance == null) {
            instance = new ManterVigilantePresenter();
        }
        view.setSize(view.getWidth(), 292);
        Centralize();
        view.setVisible(true);
        return instance;
    }

    public TelaManterVigilanteView getTela() {
        return view;
    }

    private void iniciarTela() {

        // Implementando botão buscar da tela ManterVigilante // 
        view.getBtnBuscar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {

                    if (view.getTextCPF().getText().equals("")) {
                        throw new NullPointerException();
                    }
                    //Buscando vigilante
                    PessoaDAO pdao = new PessoaDAO();
                    p = pdao.busca(view.getTextCPF().getText());

                    //Setando valores na view
                    if (p != null) {
                        view.getTextRG().setText(p.getRg());
                        view.getTextNome().setText(p.getNome());
                        view.getTextEscola().setText(p.getEscolaridade());
                        view.getTextSobrenome().setText(p.getSobrenome());
                        view.getTextCPF1().setText(p.getCpf());
                        view.getTextReli().setText(p.getReligiao());
                        view.getTextEstadoCivil().setText(p.getEstadoCivil());
                        view.getTextemail().setText(p.getEmail());
                        view.getTextTel().setText(p.getTelefone());
                        view.getTextRua().setText(p.getEndereco().getRua());
                        view.getTextBairro().setText(p.getEndereco().getBairro());
                        view.getTextEstado().setText(p.getEndereco().getCidade().getUf());
                        view.getTextCid().setText(p.getEndereco().getCidade().getNome());
                        view.getTextNum().setText(p.getEndereco().getNumero());

                        Contrato c = ContratoDAO.buscaContrato(p.getIdPessoa());
                        view.getTxtSalario().setText(Double.toString(c.getSalario()));
                        view.getTextDataIni().setText(c.getDataEntrada());
                        view.getTextDataSai().setText(c.getDataSaida());

                        String j = VigilanteDAO.busca(p.getIdPessoa());
                        if (j.equals("s")) {
                            view.getTextSituacao().setText("Admitido");
                            view.getBtnReadmite().setEnabled(false);
                            view.getBtnDemite().setEnabled(true);
                        } else if (j.equals("n")) {
                            view.getTextSituacao().setText("Demitido");
                            view.getBtnDemite().setEnabled(false);
                            view.getBtnConcluir().setEnabled(false);
                            view.getBtnReadmite().setEnabled(true);
                        } else {
                            view.getTextSituacao().setText("Sem situação");
                        }

                        view.setSize(view.getWidth(), 624);
                        Centralize();
                    }
                } catch (NullPointerException ne) {
                    JOptionPane.showMessageDialog(null, "informe um CPF para ser buscado");
                }
            }
        });

        //Implementando botão de voltar// 
        view.getBtnVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setVisible(false);
                view.dispose();
                PrincipalPresenter presenter = PrincipalPresenter.getInstance();
                instance = null;

            }
        });

        //Implementando botão de fechar
        view.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                PrincipalPresenter.getInstance();
                instance = null;
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });

        view.getBtnConcluir().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                PessoaDAO pdao = new PessoaDAO();
                p.setEstadoCivil(view.getTextEstadoCivil().getText());
                p.setEscolaridade(view.getTextEscola().getText());
                p.setTelefone(view.getTextTel().getText());
                p.setEmail(view.getTextemail().getText());
                p.setReligiao(view.getTextReli().getText());
                p.setNome(view.getTextCid().getText());
                p.getEndereco().getCidade().setNome(view.getTextCid().getText());
                p.getEndereco().getCidade().setUf(view.getTextEstado().getText());
                p.getEndereco().setBairro(view.getTextBairro().getText());
                p.getEndereco().setRua(view.getTextRua().getText());
                p.getEndereco().setNumero(view.getTextNum().getText());

                ContratoDAO.updateContrato(p.getIdPessoa(), Double.parseDouble(view.getTxtSalario().getText()));
                pdao.update(p);

            }
        });

        view.getBtnExcluir().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        //Botão para demitir vigilante
        view.getBtnDemite().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    ContratoDAO.demite(p.getIdPessoa());
                    VigilanteDAO.demite(p.getIdPessoa());
                    view.getBtnDemite().setEnabled(false);
                    view.getBtnConcluir().setEnabled(false);
                    view.getBtnReadmite().setEnabled(true);
                    String j = VigilanteDAO.busca(p.getIdPessoa());
                    if (j.equals("s")) {
                        view.getTextSituacao().setText("Admitido");
                        view.getBtnReadmite().setEnabled(false);
                        view.getBtnDemite().setEnabled(true);
                    } else if (j.equals("n")) {
                        view.getTextSituacao().setText("Demitido");
                        view.getBtnDemite().setEnabled(false);
                        view.getBtnReadmite().setEnabled(true);
                    } else {
                        view.getTextSituacao().setText("Sem situação");
                    }
                    Contrato c = ContratoDAO.buscaDemitidos(p.getIdPessoa());
                    view.getTxtSalario().setText(Double.toString(c.getSalario()));
                    view.getTextDataIni().setText(c.getDataEntrada());
                    view.getTextDataSai().setText(c.getDataSaida());
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Erro ao demitir vigilante");
                }

            }
        });

        //Botão para readmitir vigilante
        view.getBtnReadmite().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {

                    ContratoDAO.readmite(Double.parseDouble(view.getTxtSalario().getText()), p.getIdPessoa());
                    VigilanteDAO.readminte(p.getIdPessoa());
                    view.getBtnReadmite().setEnabled(false);
                    view.getBtnDemite().setEnabled(true);
                    view.getBtnConcluir().setEnabled(true);
                    String j = VigilanteDAO.busca(p.getIdPessoa());
                    if (j.equals("s")) {
                        view.getTextSituacao().setText("Admitido");
                        view.getBtnReadmite().setEnabled(false);
                        view.getBtnDemite().setEnabled(true);
                    } else if (j.equals("n")) {
                        view.getTextSituacao().setText("Demitido");
                        view.getBtnDemite().setEnabled(false);
                        view.getBtnReadmite().setEnabled(true);
                    } else {
                        view.getTextSituacao().setText("Sem situação");
                    }
                    Contrato c = ContratoDAO.buscaContrato(p.getIdPessoa());
                    view.getTxtSalario().setText(Double.toString(c.getSalario()));
                    view.getTextDataIni().setText(c.getDataEntrada());
                    view.getTextDataSai().setText(c.getDataSaida());

                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Erro ao readmitir vigilante");
                }

            }
        });

    }

    //Método para centralizar tela
    private static void Centralize() {
        Dimension ds = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dw = view.getSize();
        view.setLocation((ds.width - dw.width) / 2, (ds.height - dw.height - 30) / 2);
    }
}
