/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhobanco.presenter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import model.bean.Predio;
import model.dao.PredioDAO;
import trabalhobanco.views.MudarNomePredioView;
/**
 *
 * @author Maurício
 */
public class MudarNomePredioPresenter {
    private static MudarNomePredioView view;
    private static MudarNomePredioPresenter instance;
    private static Predio pre;
    
    private MudarNomePredioPresenter(){
        view = new MudarNomePredioView();
        this.iniciarTela();
    }
    
    private void iniciarTela(){       
        //Implementando botão de salvar novo nome de prédio
        view.getBtnSalvar().addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                PredioDAO.updateNome(view.getTxtPredio().getText(), pre);
                pre.setNome(view.getTxtPredio().getText());        
            }  
        });
        
        //Implementando botão de exit do JFrame
        view.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                ManterSetoresEPrediosPresenter.getInstance();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                instance = null;
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }

        });
        
        //Implementando botão de voltar
        view.getBtnVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ManterSetoresEPrediosPresenter.getInstance();
                view.setVisible(false);
                view.dispose();
            }
        });
    }
    
    public static void SetarPredioESetor(String setor, Predio predio){
        pre = predio;
        view.getTxtPredio().setText(pre.getNome());
        view.getTxtNomeAntigo().setText(pre.getNome());
        view.getTxtSetor().setText(setor);
    }
    
    public static MudarNomePredioPresenter getInstance(){
        if(instance == null){
            instance = new MudarNomePredioPresenter();
        }
        view.setVisible(true);
        return instance;
    }  
}
