/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhobanco.presenter;

import Supporte.Preenchedor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.OK_CANCEL_OPTION;
import model.dao.SetorDAO;
import trabalhobanco.views.MudarNomeSetorView;

/**
 *
 * @author Gabriel
 */
public class MudarNomeSetorPresenter {

    private static MudarNomeSetorView view;
    private static MudarNomeSetorPresenter instance;

    private MudarNomeSetorPresenter() {
        view = new MudarNomeSetorView();
        this.iniciarTela();
    }

    private void iniciarTela() {

        //Implementando botão de salvar
        view.getBtnSalvar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int result = JOptionPane.showConfirmDialog(null, "O nome do setor será mudado para todos os prédios dele!", "Aviso", OK_CANCEL_OPTION);
                if (result == 0) {
                    JComboBox jc = view.getjComboSetor();
                    String nomeVelho = jc.getSelectedItem().toString();
                    String nomeNovo = view.getTxtNovoNomeSetor().getText();
                    if (nomeNovo.equals(nomeVelho)) {
                        JOptionPane.showMessageDialog(null, "O setor já está com este nome!");
                        return;
                    }
                    if (SetorDAO.updateNomeSetor(nomeNovo, nomeVelho)) {
                        jc.removeAllItems();
                        Preenchedor.PreencherJComboBoxSetor(view.getjComboSetor());
                        jc.setSelectedItem(nomeNovo);
                    }
                }
            }
        });

        //Implementando botão de voltar
        view.getBtnVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ManterSetoresEPrediosPresenter.getInstance();
                view.setVisible(false);
                view.dispose();
            }
        });

        //Implementando botão de exit do JFrame
        view.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
                Preenchedor.PreencherJComboBoxSetor(view.getjComboSetor());
            }

            @Override
            public void windowClosing(WindowEvent e) {
                ManterSetoresEPrediosPresenter.getInstance();
                view.getjComboSetor().removeAllItems();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                instance = null;
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }

        });
    }

    public static MudarNomeSetorPresenter getInstance() {
        if (instance == null) {
            instance = new MudarNomeSetorPresenter();
        }
        view.setVisible(true);
        return instance;
    }

}
