/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhobanco.presenter;

import Supporte.Preenchedor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import model.bean.Predio;
import model.bean.Setor;
import model.dao.PredioDAO;
import model.dao.SetorDAO;
import trabalhobanco.views.MudarPredioDeSetorView;

/**
 *
 * @author Gabriel
 */
public class MudarPredioDeSetorPresenter {

    private static MudarPredioDeSetorView view;
    private static MudarPredioDeSetorPresenter instance;
    private static Predio pred;
    private static Setor sett = new Setor();

    private MudarPredioDeSetorPresenter() {
        view = new MudarPredioDeSetorView();
        this.iniciarTela();
    }

    private void iniciarTela() {

        //Implementando botão de voltar
        view.getBtnVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ManterSetoresEPrediosPresenter.getInstance();
                view.setVisible(false);
                view.dispose();
            }
        });

        //Implementando botão de salvar
        view.getBtnSalvar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                ArrayList<Setor> setores = SetorDAO.busca();
                sett.setNome(view.getjComboBoxSetor().getSelectedItem().toString());
                
                //Procurando o setor que quero e setando id
                for (Setor set : setores) {
                    if (sett.getNome().equals(set.getNome())) {
                        sett.setIdSetor(set.getIdSetor());
                        break;
                    }
                }
                try {
                    pred.setId(PredioDAO.buscaPredioPorNome(pred).getId());
                    if (SetorDAO.returnSet(pred.getIdSetor()).getNome().equals(MudarPredioDeSetorPresenter.getView().getjComboBoxSetor().getSelectedItem().toString())) {
                        throw new NullPointerException();
                    }
                    PredioDAO.updateIDSETOR(pred, sett);
                    JOptionPane.showMessageDialog(null, "Setor mudado com sucesso!");
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "O prédio ja está neste setor!");
                }
            }
        });

        //Implementando botão de exit do JFrame
        view.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
                
            }

            @Override
            public void windowClosing(WindowEvent e) {
                ManterSetoresEPrediosPresenter.getInstance();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                instance = null;         
            }

            @Override
            public void windowIconified(WindowEvent e) {
                
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
                
            }

            @Override
            public void windowActivated(WindowEvent e) {
                
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
                
            }
        });
    }

    public static MudarPredioDeSetorPresenter getInstance() {
        if (instance == null) {
            instance = new MudarPredioDeSetorPresenter();
        }
        view.setVisible(true);
        return instance;
    }

    public static MudarPredioDeSetorView getView() {
        return view;
    }

    public static void setarPredio(Predio pre) {
        pred = pre;
        view.getTxtPredio().setText(pred.getNome());
    }

}
