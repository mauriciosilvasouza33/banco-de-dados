package trabalhobanco.presenter;

import Supporte.Preenchedor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import model.dao.OcorrenciaDAO;
import trabalhobanco.views.ManterTurnoView;
import trabalhobanco.views.TelaPrincipalView;

public class PrincipalPresenter {

    private static TelaPrincipalView view;
    private static PrincipalPresenter instance;

    private PrincipalPresenter() {
        view = new TelaPrincipalView();
        iniciarTela();
    }

    public static PrincipalPresenter getInstance() {
        if (instance == null) {
            instance = new PrincipalPresenter();
        }
        view.setVisible(true);
        return instance;
    }

    public TelaPrincipalView getView() {
        return view;
    }

    private void iniciarTela() {
        //Implementando botão de deslogar
        view.getBtnDeslogar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setVisible(false);
                view.dispose();
                LoginPresenter.getInstance();
            }

        });
        
  
        
        //Implementando botão de abrir tela de manterVigilante
        view.getBtnVigilante().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ManterVigilantePresenter.getInstance();
                view.setVisible(false);
                view.dispose();
            }

        });

        //Implementando botão de abrir tela de manterTurno
        view.getBtnHorario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ManterTurnoPresenter.getInstance();
                
                //Atualizando a tabela com os turnos cadastrados
                ManterTurnoView axala = ManterTurnoPresenter.getView();
                CadastrarNovoTurnoPresenter.PreencherJTable(axala.getjTabelaTurno(), axala);
                
                //Atualizando a tabela de ocorrências
                OcorrenciaDAO.PreencherJTable(axala.getjTabelaOcorrencias(), axala);
                
                view.setVisible(false);
                view.dispose();
            }
        });
        
        //Implementando botão de abrir tela de cadastrarVigilante
        view.getBtnCadastro().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CadastrarPresenter.getInstance();
                view.setVisible(false);
                view.dispose();
            }
        });
        

        //Implementando botão de abrir tela ManterSetoresEPredios
        view.getBtnPredios().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ManterSetoresEPrediosPresenter.getInstance();
                view.setVisible(false);
                view.dispose();
            }
        });

        //Implementando botão de abrir tela de manter setores e prédios
        view.getBtnPredios().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ManterSetoresEPrediosPresenter.getInstance();
            }
        });
    }

}
