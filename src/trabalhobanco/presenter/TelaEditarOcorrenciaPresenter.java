/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhobanco.presenter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;
import model.bean.Ocorrencia;
import model.dao.OcorrenciaDAO;
import trabalhobanco.views.ManterTurnoView;
import trabalhobanco.views.TelaEditarOcorrenciaView;

/**
 *
 * @author Maurício
 */
public class TelaEditarOcorrenciaPresenter {
    
    private static TelaEditarOcorrenciaView view;
    private static TelaEditarOcorrenciaPresenter instance;
    
    private TelaEditarOcorrenciaPresenter(){
        view = new TelaEditarOcorrenciaView();
        this.iniciarTela();
    }

    private void iniciarTela() {
        //Implementando botão de exit do JFrame
        view.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                ManterTurnoPresenter.getInstance();
                instance = null;
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });
        
        view.getBtnVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                ManterTurnoPresenter.getInstance();
                view.setVisible(false);
                view.dispose();
                
                //Atualizando a tabela de ocorrências
                ManterTurnoView axala = ManterTurnoPresenter.getView();
                OcorrenciaDAO.PreencherJTable(axala.getjTabelaOcorrencias(), axala);
                axala.getBtnOcoEdit().setEnabled(false);
                instance = null;
               
            }
        });
        
        view.getBtnCadastrar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                //Obtendo as informações para edição de ocorrencia
                int i = ManterTurnoPresenter.getView().getjTabelaOcorrencias().getSelectedRow();
                List<Ocorrencia> o = OcorrenciaDAO.buscaOcorrencia();
                TelaEditarOcorrenciaView v = TelaEditarOcorrenciaPresenter.getView();
                
                //Setando a nova descrição
                o.get(i).setDescricao(view.getjTextDescricao().getText());
                
                //Atualizando edição de ocorrencia
                OcorrenciaDAO.updateOcorrencia(o.get(i));
                
            }
        });
    }
    
    public static TelaEditarOcorrenciaPresenter getInstance(){
        if(instance == null){
            instance = new TelaEditarOcorrenciaPresenter();
        }
        view.setVisible(true);
        return instance;
    }
    
    public static TelaEditarOcorrenciaView getView(){
        return view;
    }
    
}
