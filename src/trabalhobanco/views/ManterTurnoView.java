/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhobanco.views;

import javax.swing.JButton;
import javax.swing.JTable;

/**
 *
 * @author Maurício
 */
public class ManterTurnoView extends javax.swing.JFrame {

    public JButton getBtnGerarRelatorio() {
        return btnGerarRelatorio;
    }

    
    
    public JTable getjTabelaTurno() {
        return jTabelaTurno;
    }

    
    public JButton getBtnOcoEdit() {
        return btnOcoEdit;
    }

    /**
     * Creates new form TelaEditar
     */
    public void setBtnOcoEdit(JButton btnOcoEdit) {    
        this.btnOcoEdit = btnOcoEdit;
    }

    public void setjTabelaTurno(JTable jTabelaTurno) {
        this.jTabelaTurno = jTabelaTurno;
    }

    public ManterTurnoView() {
        initComponents();
    }

    public JButton getBtnVoltar() {
        return btnVoltar;
    }

    public JButton getBtnTurnoCadastro() {
        return btnTurnoCadastro;
    }

    public void setBtnTurnoCadastro(JButton btnTurnoCadastro) {
        this.btnTurnoCadastro = btnTurnoCadastro;
    }

    public JButton getBtnNovaOcorrencia() {
        return btnNovaOcorrencia;
    }

    public void setBtnNovaOcorrencia(JButton btnNovaOcorrencia) {
        this.btnNovaOcorrencia = btnNovaOcorrencia;
    }

    public JTable getjTabelaOcorrencias() {
        return jTabelaOcorrencias;
    }

    public void setjTabelaOcorrencias(JTable jTabelaOcorrencias) {
        this.jTabelaOcorrencias = jTabelaOcorrencias;
    }

    public JButton getBtnDelRonda() {
        return btnDelRonda;
    }

    public void setBtnDelRonda(JButton btnDelRonda) {
        this.btnDelRonda = btnDelRonda;
    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCorpo = new javax.swing.JPanel();
        panelDesign = new javax.swing.JPanel();
        txtDesign = new javax.swing.JTextField();
        lblHorario = new javax.swing.JLabel();
        btnVoltar = new javax.swing.JButton();
        jPanelBusca3 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jScroll = new javax.swing.JScrollPane();
        jTabelaOcorrencias = new javax.swing.JTable();
        btnOcoEdit = new javax.swing.JButton();
        btnGerarRelatorio = new javax.swing.JButton();
        jPanelBusca4 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jScroll1 = new javax.swing.JScrollPane();
        jTabelaTurno = new javax.swing.JTable();
        btnTurnoCadastro = new javax.swing.JButton();
        btnNovaOcorrencia = new javax.swing.JButton();
        btnDelRonda = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Manter turno");
        setResizable(false);

        jPanelCorpo.setBackground(new java.awt.Color(27, 187, 125));

        panelDesign.setBackground(new java.awt.Color(255, 255, 255));
        panelDesign.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(51, 51, 51)));
        panelDesign.setPreferredSize(new java.awt.Dimension(680, 133));

        txtDesign.setEditable(false);
        txtDesign.setBackground(new java.awt.Color(255, 255, 255));
        txtDesign.setFont(new java.awt.Font("DialogInput", 1, 24)); // NOI18N
        txtDesign.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtDesign.setText("MANTER TURNO");
        txtDesign.setBorder(null);
        txtDesign.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDesignActionPerformed(evt);
            }
        });

        lblHorario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/horario.png"))); // NOI18N

        btnVoltar.setBackground(new java.awt.Color(84, 80, 80));
        btnVoltar.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnVoltar.setForeground(new java.awt.Color(255, 255, 255));
        btnVoltar.setText("Voltar");
        btnVoltar.setAlignmentX(0.5F);
        btnVoltar.setBorderPainted(false);
        btnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoltarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelDesignLayout = new javax.swing.GroupLayout(panelDesign);
        panelDesign.setLayout(panelDesignLayout);
        panelDesignLayout.setHorizontalGroup(
            panelDesignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDesignLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblHorario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDesign, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(121, 121, 121)
                .addComponent(btnVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        panelDesignLayout.setVerticalGroup(
            panelDesignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDesignLayout.createSequentialGroup()
                .addGroup(panelDesignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDesignLayout.createSequentialGroup()
                        .addGap(69, 69, 69)
                        .addComponent(btnVoltar))
                    .addGroup(panelDesignLayout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGroup(panelDesignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDesign, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblHorario, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanelBusca3.setBackground(new java.awt.Color(255, 255, 255));
        jPanelBusca3.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(51, 51, 51)));

        jLabel10.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel10.setText("Registros de ocorrências:");

        jLabel6.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jTabelaOcorrencias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Vigilante", "CPF", "Data", "Periodo", "Prédio", "Ocorrência"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTabelaOcorrencias.getTableHeader().setReorderingAllowed(false);
        jScroll.setViewportView(jTabelaOcorrencias);

        btnOcoEdit.setBackground(new java.awt.Color(84, 80, 80));
        btnOcoEdit.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnOcoEdit.setForeground(new java.awt.Color(255, 255, 255));
        btnOcoEdit.setText("Editar ocorrência");
        btnOcoEdit.setAlignmentX(0.5F);
        btnOcoEdit.setBorderPainted(false);
        btnOcoEdit.setEnabled(false);
        btnOcoEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOcoEditActionPerformed(evt);
            }
        });

        btnGerarRelatorio.setBackground(new java.awt.Color(84, 80, 80));
        btnGerarRelatorio.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnGerarRelatorio.setForeground(new java.awt.Color(255, 255, 255));
        btnGerarRelatorio.setText("Gerar relatório de ocorrência");
        btnGerarRelatorio.setAlignmentX(0.5F);
        btnGerarRelatorio.setBorderPainted(false);
        btnGerarRelatorio.setEnabled(false);
        btnGerarRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGerarRelatorioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelBusca3Layout = new javax.swing.GroupLayout(jPanelBusca3);
        jPanelBusca3.setLayout(jPanelBusca3Layout);
        jPanelBusca3Layout.setHorizontalGroup(
            jPanelBusca3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBusca3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelBusca3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 662, Short.MAX_VALUE)
                    .addGroup(jPanelBusca3Layout.createSequentialGroup()
                        .addGroup(jPanelBusca3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel10)
                            .addGroup(jPanelBusca3Layout.createSequentialGroup()
                                .addComponent(btnOcoEdit)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnGerarRelatorio)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanelBusca3Layout.setVerticalGroup(
            jPanelBusca3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBusca3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanelBusca3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOcoEdit)
                    .addComponent(btnGerarRelatorio))
                .addGap(99, 99, 99)
                .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanelBusca4.setBackground(new java.awt.Color(255, 255, 255));
        jPanelBusca4.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(51, 51, 51)));

        jLabel11.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel11.setText("Registros de turnos e suas respectivas rondas nos setores:");

        jLabel12.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jTabelaTurno.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Vigilante", "Data", "Período", "Setor"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTabelaTurno.getTableHeader().setReorderingAllowed(false);
        jScroll1.setViewportView(jTabelaTurno);

        btnTurnoCadastro.setBackground(new java.awt.Color(84, 80, 80));
        btnTurnoCadastro.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnTurnoCadastro.setForeground(new java.awt.Color(255, 255, 255));
        btnTurnoCadastro.setText("Cadastrar novo turno");
        btnTurnoCadastro.setAlignmentX(0.5F);
        btnTurnoCadastro.setBorderPainted(false);
        btnTurnoCadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTurnoCadastroActionPerformed(evt);
            }
        });

        btnNovaOcorrencia.setBackground(new java.awt.Color(84, 80, 80));
        btnNovaOcorrencia.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnNovaOcorrencia.setForeground(new java.awt.Color(255, 255, 255));
        btnNovaOcorrencia.setText("Cadastrar nova ocorrência");
        btnNovaOcorrencia.setToolTipText("Selecione um turno para cadastro de uma ocorrência");
        btnNovaOcorrencia.setAlignmentX(0.5F);
        btnNovaOcorrencia.setBorderPainted(false);
        btnNovaOcorrencia.setEnabled(false);
        btnNovaOcorrencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovaOcorrenciaActionPerformed(evt);
            }
        });

        btnDelRonda.setBackground(new java.awt.Color(84, 80, 80));
        btnDelRonda.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnDelRonda.setForeground(new java.awt.Color(255, 255, 255));
        btnDelRonda.setText("Apagar registro de ronda");
        btnDelRonda.setAlignmentX(0.5F);
        btnDelRonda.setBorderPainted(false);
        btnDelRonda.setEnabled(false);
        btnDelRonda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelRondaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelBusca4Layout = new javax.swing.GroupLayout(jPanelBusca4);
        jPanelBusca4.setLayout(jPanelBusca4Layout);
        jPanelBusca4Layout.setHorizontalGroup(
            jPanelBusca4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBusca4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelBusca4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelBusca4Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanelBusca4Layout.createSequentialGroup()
                        .addGroup(jPanelBusca4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelBusca4Layout.createSequentialGroup()
                                .addComponent(btnTurnoCadastro)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnNovaOcorrencia, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnDelRonda, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 36, Short.MAX_VALUE))
                            .addComponent(jScroll1))
                        .addGap(0, 0, 0)
                        .addComponent(jLabel12)
                        .addContainerGap())))
        );
        jPanelBusca4Layout.setVerticalGroup(
            jPanelBusca4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBusca4Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanelBusca4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelBusca4Layout.createSequentialGroup()
                        .addGap(130, 130, 130)
                        .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanelBusca4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScroll1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11)
                        .addGroup(jPanelBusca4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnDelRonda)
                            .addGroup(jPanelBusca4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnTurnoCadastro)
                                .addComponent(btnNovaOcorrencia)))
                        .addGap(0, 14, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout jPanelCorpoLayout = new javax.swing.GroupLayout(jPanelCorpo);
        jPanelCorpo.setLayout(jPanelCorpoLayout);
        jPanelCorpoLayout.setHorizontalGroup(
            jPanelCorpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCorpoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelCorpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelBusca4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelDesign, javax.swing.GroupLayout.DEFAULT_SIZE, 686, Short.MAX_VALUE)
                    .addComponent(jPanelBusca3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanelCorpoLayout.setVerticalGroup(
            jPanelCorpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCorpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelDesign, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelBusca4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelBusca3, javax.swing.GroupLayout.PREFERRED_SIZE, 229, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelCorpo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelCorpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txtDesignActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDesignActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDesignActionPerformed

    private void btnNovaOcorrenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovaOcorrenciaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnNovaOcorrenciaActionPerformed

    private void btnTurnoCadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTurnoCadastroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnTurnoCadastroActionPerformed

    private void btnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoltarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnVoltarActionPerformed

    private void btnOcoEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOcoEditActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnOcoEditActionPerformed

    private void btnDelRondaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelRondaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnDelRondaActionPerformed

    private void btnGerarRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGerarRelatorioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGerarRelatorioActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ManterTurnoView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ManterTurnoView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ManterTurnoView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ManterTurnoView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ManterTurnoView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDelRonda;
    private javax.swing.JButton btnGerarRelatorio;
    private javax.swing.JButton btnNovaOcorrencia;
    private javax.swing.JButton btnOcoEdit;
    private javax.swing.JButton btnTurnoCadastro;
    private javax.swing.JButton btnVoltar;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanelBusca3;
    private javax.swing.JPanel jPanelBusca4;
    private javax.swing.JPanel jPanelCorpo;
    private javax.swing.JScrollPane jScroll;
    private javax.swing.JScrollPane jScroll1;
    private javax.swing.JTable jTabelaOcorrencias;
    private javax.swing.JTable jTabelaTurno;
    private javax.swing.JLabel lblHorario;
    private javax.swing.JPanel panelDesign;
    private javax.swing.JTextField txtDesign;
    // End of variables declaration//GEN-END:variables
}