/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhobanco.views;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Gabriel
 */
public class TelaPrincipalView extends javax.swing.JFrame {

    /**
     * Creates new form TelaPrincipal
     */
    public TelaPrincipalView() {
        initComponents();
    }



  



    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCorpo = new javax.swing.JPanel();
        panelDesign = new javax.swing.JPanel();
        txtDesign = new javax.swing.JTextField();
        lblDesign = new javax.swing.JLabel();
        btnDeslogar = new javax.swing.JButton();
        btnPredios = new javax.swing.JButton();
        panelButoes = new javax.swing.JPanel();
        panelHorario = new javax.swing.JPanel();
        btnHorario = new javax.swing.JButton();
        lblHorario = new javax.swing.JLabel();
        txtHorario = new javax.swing.JLabel();
        panelCadastro = new javax.swing.JPanel();
        txtCadastro = new javax.swing.JLabel();
        lblCadastro = new javax.swing.JLabel();
        btnCadastro = new javax.swing.JButton();
        panelVigilante = new javax.swing.JPanel();
        btnVigilante = new javax.swing.JButton();
        txtVigilante = new javax.swing.JLabel();
        lblVigilante = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Controle de Vigilantes");
        setBackground(new java.awt.Color(10, 0, 0));
        setFont(new java.awt.Font("Aharoni", 2, 10)); // NOI18N
        setForeground(java.awt.Color.white);
        setResizable(false);

        jPanelCorpo.setBackground(new java.awt.Color(27, 187, 125));

        panelDesign.setBackground(new java.awt.Color(255, 255, 255));
        panelDesign.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(51, 51, 51)));
        panelDesign.setPreferredSize(new java.awt.Dimension(680, 133));

        txtDesign.setEditable(false);
        txtDesign.setBackground(new java.awt.Color(255, 255, 255));
        txtDesign.setFont(new java.awt.Font("DialogInput", 1, 36)); // NOI18N
        txtDesign.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtDesign.setText("CONTROLE DE VIGILANTES");
        txtDesign.setBorder(null);
        txtDesign.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDesignActionPerformed(evt);
            }
        });

        lblDesign.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/iconeVigilante.png"))); // NOI18N

        btnDeslogar.setBackground(new java.awt.Color(84, 80, 80));
        btnDeslogar.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnDeslogar.setForeground(new java.awt.Color(255, 255, 255));
        btnDeslogar.setText("Deslogar");
        btnDeslogar.setAlignmentX(0.5F);
        btnDeslogar.setBorderPainted(false);

        btnPredios.setBackground(new java.awt.Color(84, 80, 80));
        btnPredios.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnPredios.setForeground(new java.awt.Color(255, 255, 255));
        btnPredios.setText("Ver setores e prédios");
        btnPredios.setAlignmentX(0.5F);
        btnPredios.setBorderPainted(false);
        btnPredios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrediosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelDesignLayout = new javax.swing.GroupLayout(panelDesign);
        panelDesign.setLayout(panelDesignLayout);
        panelDesignLayout.setHorizontalGroup(
            panelDesignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDesignLayout.createSequentialGroup()
                .addContainerGap(21, Short.MAX_VALUE)
                .addComponent(lblDesign)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelDesignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDesign, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelDesignLayout.createSequentialGroup()
                        .addComponent(btnDeslogar, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPredios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(24, 24, 24))
        );
        panelDesignLayout.setVerticalGroup(
            panelDesignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDesignLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(panelDesignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDesignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(panelDesignLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnDeslogar)
                            .addComponent(btnPredios))
                        .addComponent(lblDesign))
                    .addComponent(txtDesign, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(43, Short.MAX_VALUE))
        );

        panelButoes.setBackground(new java.awt.Color(255, 255, 255));
        panelButoes.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(51, 51, 51)));

        panelHorario.setBackground(new java.awt.Color(238, 236, 236));
        panelHorario.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(153, 153, 153), new java.awt.Color(204, 204, 204)));
        panelHorario.setPreferredSize(new java.awt.Dimension(116, 132));

        btnHorario.setBackground(new java.awt.Color(84, 80, 80));
        btnHorario.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnHorario.setForeground(new java.awt.Color(255, 255, 255));
        btnHorario.setText("Manter");
        btnHorario.setAlignmentX(0.5F);
        btnHorario.setBorderPainted(false);
        btnHorario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHorarioActionPerformed(evt);
            }
        });

        lblHorario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/horario.png"))); // NOI18N

        txtHorario.setFont(new java.awt.Font("Franklin Gothic Heavy", 0, 12)); // NOI18N
        txtHorario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtHorario.setText("Manter Turno");

        javax.swing.GroupLayout panelHorarioLayout = new javax.swing.GroupLayout(panelHorario);
        panelHorario.setLayout(panelHorarioLayout);
        panelHorarioLayout.setHorizontalGroup(
            panelHorarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHorarioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelHorarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnHorario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtHorario, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(panelHorarioLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(lblHorario)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelHorarioLayout.setVerticalGroup(
            panelHorarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHorarioLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblHorario, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtHorario, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnHorario)
                .addGap(28, 28, 28))
        );

        panelCadastro.setBackground(new java.awt.Color(238, 236, 236));
        panelCadastro.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(153, 153, 153), new java.awt.Color(204, 204, 204)));
        panelCadastro.setPreferredSize(new java.awt.Dimension(116, 132));

        txtCadastro.setFont(new java.awt.Font("Franklin Gothic Heavy", 0, 12)); // NOI18N
        txtCadastro.setText("Cadastrar Vigilante");

        lblCadastro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/addVigilante.png"))); // NOI18N

        btnCadastro.setBackground(new java.awt.Color(84, 80, 80));
        btnCadastro.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnCadastro.setForeground(new java.awt.Color(255, 255, 255));
        btnCadastro.setText("Cadastrar");
        btnCadastro.setAlignmentX(0.5F);
        btnCadastro.setBorderPainted(false);
        btnCadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastroActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelCadastroLayout = new javax.swing.GroupLayout(panelCadastro);
        panelCadastro.setLayout(panelCadastroLayout);
        panelCadastroLayout.setHorizontalGroup(
            panelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCadastroLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(lblCadastro)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelCadastroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCadastro, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                    .addComponent(btnCadastro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelCadastroLayout.setVerticalGroup(
            panelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCadastroLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(lblCadastro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCadastro)
                .addGap(28, 28, 28))
        );

        panelVigilante.setBackground(new java.awt.Color(238, 236, 236));
        panelVigilante.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(153, 153, 153), new java.awt.Color(204, 204, 204)));
        panelVigilante.setPreferredSize(new java.awt.Dimension(116, 132));

        btnVigilante.setBackground(new java.awt.Color(84, 80, 80));
        btnVigilante.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnVigilante.setForeground(new java.awt.Color(255, 255, 255));
        btnVigilante.setText("Manter");
        btnVigilante.setAlignmentX(0.5F);
        btnVigilante.setBorderPainted(false);

        txtVigilante.setFont(new java.awt.Font("Franklin Gothic Heavy", 0, 12)); // NOI18N
        txtVigilante.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtVigilante.setText("Manter vigilantes");

        lblVigilante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/editVigilante.png"))); // NOI18N

        javax.swing.GroupLayout panelVigilanteLayout = new javax.swing.GroupLayout(panelVigilante);
        panelVigilante.setLayout(panelVigilanteLayout);
        panelVigilanteLayout.setHorizontalGroup(
            panelVigilanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVigilanteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelVigilanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnVigilante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtVigilante, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(panelVigilanteLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(lblVigilante)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelVigilanteLayout.setVerticalGroup(
            panelVigilanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVigilanteLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVigilante)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtVigilante, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnVigilante)
                .addGap(28, 28, 28))
        );

        javax.swing.GroupLayout panelButoesLayout = new javax.swing.GroupLayout(panelButoes);
        panelButoes.setLayout(panelButoesLayout);
        panelButoesLayout.setHorizontalGroup(
            panelButoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelButoesLayout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(panelCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addComponent(panelHorario, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addComponent(panelVigilante, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38))
        );
        panelButoesLayout.setVerticalGroup(
            panelButoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelButoesLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(panelButoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(panelVigilante, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                    .addComponent(panelHorario, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                    .addComponent(panelCadastro, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelCorpoLayout = new javax.swing.GroupLayout(jPanelCorpo);
        jPanelCorpo.setLayout(jPanelCorpoLayout);
        jPanelCorpoLayout.setHorizontalGroup(
            jPanelCorpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCorpoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelCorpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelButoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelDesign, javax.swing.GroupLayout.PREFERRED_SIZE, 623, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanelCorpoLayout.setVerticalGroup(
            jPanelCorpoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCorpoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelDesign, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelButoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanelCorpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanelCorpo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txtDesignActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDesignActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDesignActionPerformed

    private void btnHorarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHorarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnHorarioActionPerformed

    private void btnPrediosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrediosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPrediosActionPerformed

    private void btnCadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCadastroActionPerformed

    public JButton getBtnPredios() {
        return btnPredios;
    }
    
    
    public JPanel getjPanelCorpo() {
        return jPanelCorpo;
    }

    public JPanel getjPanelButoes() {
        return panelButoes;
    }

    public JTextField getjTextField1() {
        return txtDesign;
    }

    public JPanel getPanelDesign() {
        return panelDesign;
    }

    public JButton getBtnCadastro() {
        return btnCadastro;
    }

    public JButton getBtnDeslogar() {
        return btnDeslogar;
    }

    public JButton getBtnHorario() {
        return btnHorario;
    }

    public JButton getBtnVigilante() {
        return btnVigilante;
    }

    public JLabel getLblCadastro() {
        return lblCadastro;
    }

    public JLabel getLblDesign() {
        return lblDesign;
    }

    public JLabel getLblHorario() {
        return lblHorario;
    }

    public JLabel getLblVigilante() {
        return lblVigilante;
    }

    public JPanel getPanelButoes() {
        return panelButoes;
    }

    public JPanel getPanelCadastro() {
        return panelCadastro;
    }

    public JPanel getPanelHorario() {
        return panelHorario;
    }

    public JPanel getPanelVigilante() {
        return panelVigilante;
    }

    public JLabel getTxtCadastro() {
        return txtCadastro;
    }

    public JTextField getTxtDesign() {
        return txtDesign;
    }

    public JLabel getTxtHorario() {
        return txtHorario;
    }

    public JLabel getTxtVigilante() {
        return txtVigilante;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipalView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipalView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipalView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipalView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        //</editor-fold>

        /* Create and display the form */
        
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCadastro;
    private javax.swing.JButton btnDeslogar;
    private javax.swing.JButton btnHorario;
    private javax.swing.JButton btnPredios;
    private javax.swing.JButton btnVigilante;
    private javax.swing.JPanel jPanelCorpo;
    private javax.swing.JLabel lblCadastro;
    private javax.swing.JLabel lblDesign;
    private javax.swing.JLabel lblHorario;
    private javax.swing.JLabel lblVigilante;
    private javax.swing.JPanel panelButoes;
    private javax.swing.JPanel panelCadastro;
    private javax.swing.JPanel panelDesign;
    private javax.swing.JPanel panelHorario;
    private javax.swing.JPanel panelVigilante;
    private javax.swing.JLabel txtCadastro;
    private javax.swing.JTextField txtDesign;
    private javax.swing.JLabel txtHorario;
    private javax.swing.JLabel txtVigilante;
    // End of variables declaration//GEN-END:variables
}
